﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Globalization;
using System.Drawing;
using System.IO;
using System.ComponentModel;
using System.Drawing.Imaging;
using MonoStrategy;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    /// <summary>
    /// Interaction logic for AnimLibraryTab.xaml
    /// </summary>
    public partial class AnimLibraryTab : UserControl
    {


        public object Library { get { return TLibrary; } }
        public object CurrentClass { get { return TCurrentClass; } }
        public object CurrentSet { get { return TCurrentSet; } }
        public object CurrentAnim { get { return TCurrentAnim; } }

        private System.Drawing.Bitmap AmbientPreview
        {
            get
            {
                if ((TCurrentClass == null) || (TCurrentClass.Sets.Count == 0) || (TCurrentClass.Sets[0].Animations.Count == 0) ||
                        (TCurrentClass.Sets[0].Animations[0].Frames.Count == 0))
                    return null;

                return TCurrentClass.Sets[0].Animations[0].Frames[0].Source;
            }
        }

        internal AnimationLibrary TLibrary { get; private set; }
        internal AnimationClass TCurrentClass{
            get
            {
                if (TAB_ClassDetails == null)
                    return null;

                if (TAB_ClassDetails.DataContext is AnimationClass)
                    return (AnimationClass)TAB_ClassDetails.DataContext;
                else
                    return null;
            }
        } 
        internal AnimationSet TCurrentSet{
            get
            {
                if (TAB_AnimSetDetails == null)
                    return null;

                if (TAB_AnimSetDetails.DataContext is AnimationSet)
                    return (AnimationSet)TAB_AnimSetDetails.DataContext;
                else
                    return null;
            }
        }  
        internal Animation TCurrentAnim { get { return TAB_AnimDetails.DataContext as Animation; } }

        public BindingList<PointDouble> ResourceStacks { get { return null; } }
        public BindingList<RectangleDouble> SolidBoundaries { get { return null; } }

        private static readonly byte[] EmptyBitmapBytes;
        private GLRenderer m_AnimationPlayer;

        public AnimLibraryTab()
        {
            InitializeComponent();

            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeAnimationPlayer();

                TLibrary = AnimationLibrary.OpenOrCreate(@"./AnimationDirectory");
                DataContext = this;
            }

            COMBO_AnimResource.ItemsSource = Enum.GetValues(typeof(Resource));
            COMBO_AnimResource.SelectedIndex = 0;
        }

        static AnimLibraryTab()
        {
            Bitmap empty = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            empty.SetPixel(0, 0, System.Drawing.Color.FromArgb(0, 0, 0, 0));

            MemoryStream stream = new MemoryStream();

            empty.Save(stream, ImageFormat.Png);

            EmptyBitmapBytes = stream.ToArray();
        }

        public void Close()
        {
            TLibrary.Save();
            m_AnimationPlayer.Dispose();
        }

        private void CHECK_UseAmbientSet_Checked(object sender, RoutedEventArgs e)
        {
            if (!CHECK_UseAmbientSet.IsChecked.Value)
                COMBO_ClassAmbientSet.SelectedItem = null;
            else
            {
                if (TCurrentClass.Sets.Count == 0)
                    CHECK_UseAmbientSet.IsChecked = false;
                else
                    COMBO_ClassAmbientSet.SelectedItem = TCurrentClass.Sets.First();
            }
        }

        private void COMBO_ClassAmbientSet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (COMBO_ClassAmbientSet.SelectedItem != null)
                CHECK_UseAmbientSet.IsChecked = true;
        }

        private void BTN_AnimZoomDefault_Click(object sender, RoutedEventArgs e)
        {
            SLIDER_AnimZoom.Value = 1;
        }

        private void EVENT_UpdateAnimationPlayer(object sender, RoutedEventArgs e)
        {
            UpdateAnimationPlayer();

            e.Handled = true;
        }

        private void LIST_AnimFrames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selFrame = LIST_AnimFrames.SelectedValue as AnimationFrame;

            if (selFrame == null)
            {
                EDIT_FrameX.Text = "";
                EDIT_FrameY.Text = "";
                EDIT_FrameX.IsEnabled = false;
                EDIT_FrameY.IsEnabled = false;
            }
            else
            {
                EDIT_FrameX.Text = selFrame.OffsetX.ToString();
                EDIT_FrameY.Text = selFrame.OffsetY.ToString();
                EDIT_FrameX.IsEnabled = true;
                EDIT_FrameY.IsEnabled = true;
            }
        }

        private void EDIT_FrameX_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (LIST_AnimFrames.SelectedValue == null)
                return;

            (LIST_AnimFrames.SelectedValue as AnimationFrame).OffsetX = Int32.Parse(EDIT_FrameX.Text);
            TCurrentAnim.ComputeDimension();
        }

        private void EDIT_FrameY_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (LIST_AnimFrames.SelectedValue == null)
                return;

            (LIST_AnimFrames.SelectedValue as AnimationFrame).OffsetY = Int32.Parse(EDIT_FrameY.Text);
            TCurrentAnim.ComputeDimension();
        }

    }
}
