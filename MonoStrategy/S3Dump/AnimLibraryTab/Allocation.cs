﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Globalization;
using System.Drawing;
using System.IO;
using System.ComponentModel;
using System.Drawing.Imaging;
using MonoStrategy;

namespace MonoStrategy
{
    /// <summary>
    /// Interaction logic for AnimLibraryTab.xaml
    /// </summary>
    public partial class AnimLibraryTab : UserControl
    {
        private void BTN_CreateClass_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TLibrary.AddClass(EDIT_NewClassName.Text);
        }

        private void BTN_RemoveClass_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TLibrary.RemoveClass(TCurrentClass);
        }

        private void BTN_CreateSet_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TCurrentClass.AddAnimationSet(EDIT_NewSetName.Text);
        }

        private void BTN_ShowLibraryDetails_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            WIZARD_Details.SelectedItem = TAB_LibDetails;
            ROW_AnimFrameView.Height = new GridLength(0);
            GROUP_AnimDetails.Visibility = System.Windows.Visibility.Hidden;
        }

        private void BTN_CreateAnimation_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TCurrentSet.AddAnimation(EDIT_NewAnimName.Text);
        }

        private void BTN_RemoveFrame_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            if (LIST_AnimFrames.SelectedItem != null)
                TCurrentAnim.RemoveFrame((AnimationFrame)LIST_AnimFrames.SelectedItem); 
        }

        private void BTN_RemoveAnim_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            if (TCurrentAnim.SetOrNull != null)
                TCurrentAnim.SetOrNull.RemoveAnimation(TCurrentAnim);
        }

        private void BTN_RenameAnim_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TCurrentAnim.SetOrNull.Rename(TCurrentAnim, EDIT_RenameAnim.Text);
        }

        private void BTN_RemoveSet_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TCurrentSet.Class.RemoveAnimationSet(TCurrentSet);
        }

        private void BTN_RenameSet_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TCurrentSet.Class.Rename(TCurrentSet, EDIT_SetRename.Text);
        }

        private void BTN_ClassRename_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TCurrentClass.Library.Rename(TCurrentClass, EDIT_ClassRename.Text);
        }

        private void BTN_AddAudio_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            TLibrary.AddAudio(EDIT_AudioName.Text, File.ReadAllBytes(EDIT_AudioPath.Text));
        }

        private void BTN_RemoveAudio_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            AudioObject audio = LIST_AudioObjects.SelectedItem as AudioObject;

            if (audio != null)
                TLibrary.RemoveAudio(audio);
        }

        private void BTN_Play_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            AudioObject audio = LIST_AudioObjects.SelectedItem as AudioObject;

            if (audio != null)
                audio.CreatePlayer(null).Play(0);
        }
    }
}
