﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Globalization;
using System.Drawing;
using System.IO;
using System.ComponentModel;
using System.Drawing.Imaging;
using MonoStrategy;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    /// <summary>
    /// Interaction logic for AnimLibraryTab.xaml
    /// </summary>
    public partial class AnimLibraryTab : UserControl
    {

        /// <summary>
        /// Produces an image which is upscaled by a factor of 4. This requires the processor "hq4x.exe"
        /// to be in the application directory!
        /// </summary>
        /// <param name="inSource"></param>
        /// <returns></returns>
        private static Bitmap Upscale(Bitmap inSource)
        {
            if (!File.Exists(".\\hq4x.exe"))
                throw new NotSupportedException("The required processor \"hq4x.exe\" is missing.");

            String srcPath = System.IO.Path.GetTempFileName();
            String dstPath = System.IO.Path.GetTempFileName();

            try
            {
                inSource.Save(srcPath, ImageFormat.Bmp);

                var hq4x = System.Diagnostics.Process.Start(".\\hq4x.exe", "\"" + srcPath + "\" \"" + dstPath + "\"");

                if (!hq4x.WaitForExit(10000))
                {
                    hq4x.Kill();

                    throw new ApplicationException("Scaling could not be completed, since processor \"hq4x.exe\" did not complete in reasonable time.");
                }

                try
                {
                    hq4x.Kill();
                }
                catch { }

                MemoryStream data = new MemoryStream(File.ReadAllBytes(dstPath));

                return (Bitmap)Bitmap.FromStream(data);
            }
            finally
            {
                try { File.Delete(srcPath); }
                catch { }
                try { File.Delete(dstPath); }
                catch { }
            }
        }
    }
}
