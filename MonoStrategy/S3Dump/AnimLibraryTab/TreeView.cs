﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Globalization;
using System.Drawing;
using System.IO;
using System.ComponentModel;
using System.Drawing.Imaging;
using MonoStrategy;

namespace MonoStrategy
{
    /// <summary>
    /// Interaction logic for AnimLibraryTab.xaml
    /// </summary>
    public partial class AnimLibraryTab : UserControl
    {
        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            object item;

            if (e != null)
            {
                e.Handled = true;

                item = e.NewValue;
            }
            else
                item = sender;
           
            if (item is Animation)
            {
                Animation anim = item as Animation;

                WIZARD_Details.SelectedItem = TAB_AnimDetails;
                TAB_AnimDetails.DataContext = null;
                TAB_AnimDetails.DataContext = anim;
                GROUP_AnimDetails.DataContext = null;
                GROUP_AnimDetails.DataContext = anim;
                GROUP_AnimDetails.Header = "Details For Animation \"" + anim.Name + "\":";
                GROUP_AnimDetails.Visibility = System.Windows.Visibility.Visible;
                ROW_AnimFrameView.Height = new GridLength(GROUP_AnimDetails.Height);

                // also update animation set if changed
                if (anim.SetOrNull != TCurrentSet)
                {
                    TAB_AnimSetDetails.DataContext = null;
                    TAB_AnimSetDetails.DataContext = anim.SetOrNull;

                    if (anim.SetOrNull.Class != TCurrentClass)
                    {
                        TAB_ClassDetails.DataContext = null;
                        TAB_ClassDetails.DataContext = anim.SetOrNull.Class;
                    }
                }
            }

            if (item is AnimationSet)
            {
                AnimationSet animSet = item as AnimationSet;

                WIZARD_Details.SelectedItem = TAB_AnimSetDetails;
                TAB_AnimSetDetails.DataContext = null;
                TAB_AnimSetDetails.DataContext = animSet;
                GROUP_AnimDetails.Visibility = System.Windows.Visibility.Hidden;
                ROW_AnimFrameView.Height = new GridLength(0);

                // also update animation class if changed
                if (animSet.Class != TCurrentClass)
                {
                    TAB_ClassDetails.DataContext = null;
                    TAB_ClassDetails.DataContext = animSet.Class;
                }
            }

            if (item is AnimationClass)
            {
                if (TCurrentClass != null)
                    TCurrentClass.AmbientSet = (AnimationSet)COMBO_ClassAmbientSet.SelectedItem;

                AnimationClass animClass = item as AnimationClass;

                WIZARD_Details.SelectedItem = TAB_ClassDetails;
                TAB_AnimSetDetails.DataContext = null;
                TAB_AnimDetails.DataContext = null;
                TAB_ClassDetails.DataContext = null;
                TAB_ClassDetails.DataContext = animClass;
                COMBO_ClassAmbientSet.SelectedItem = animClass.AmbientSet;
                CHECK_UseAmbientSet.IsChecked = animClass.AmbientSet != null;
                ROW_AnimFrameView.Height = new GridLength(0);
                GROUP_AnimDetails.Visibility = System.Windows.Visibility.Hidden;
            }

            TAB_StacksAndPlane.DataContext = null;
            TAB_StacksAndPlane.DataContext = this;

            UpdateAnimationPlayer();
        }
    }
}
