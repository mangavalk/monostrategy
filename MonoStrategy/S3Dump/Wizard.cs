﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace MonoStrategy
{
    public class Wizard : TabControl
    {
        private Style m_Style;

        public Wizard(){
            m_Style = new Style(typeof(TabItem));
            Setter setter = new Setter(TabItem.TemplateProperty, new ControlTemplate(typeof(TabItem)));

            m_Style.Setters.Add(setter);

            Loaded += new RoutedEventHandler(Wizard_Loaded);

            Background = Brushes.Transparent;
            BorderThickness = new Thickness(0,0,0,0);
        }

        void Wizard_Loaded(object sender, RoutedEventArgs eargs)
        {
            foreach (var e in Items)
            {
                (e as TabItem).Style = m_Style;
            }
        }
    }
}
