﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

[assembly: Obfuscation(Feature = "encrypt symbol names with password PylzUywGXZkES5r9Hiad", Exclude = false)]

namespace MonoStrategy
{
    public static partial class Program
    {
        [STAThread]
        public static void Main(string[] inArgs)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            VisualUtilities.Instance = new AnimationUtilities();

            try
            {
                Config = XMLConfig.Load("./MonoStrategy.config.xml");

                CheckSettlersInstallation();

                XMLConfig.Save(Config, "./MonoStrategy.config.xml");

                // parse command line arguments
                if (inArgs.Length >= 1)
                {
                    switch (inArgs[0])
                    {
                        case "atlas:generate": GenerateTextureAtlas(); return;
                        case "atlas:createcache": CreateTextureAtlasCache(); return;
                        default: 
                            break; // run the game!
                    }
                }



                Log.OnCriticalException += (exception) =>
                {
                    CurrentDomain_UnhandledException(null, null);
                };

                System.Threading.Thread thread = new System.Threading.Thread(() =>
                {
                    Initialize();

                    Renderer.WaitForTermination();
                });

                thread.IsBackground = true;
                thread.Start();
                thread.Join();
            }
            catch (Exception e)
            {
                Log.LogExceptionCritical(e);
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                if (e != null)
                {
                    Log.LogException((Exception)e.ExceptionObject);
                }
            }
            catch
            {
            }

            try
            {
                StoreGameState(true);
            }
            catch
            {
            }
        }

        public static void StoreGameState(bool inFromException)
        {
            // store current game state
            var map = Map;

            if (map != null)
            {
                String filename = "./GameLog." + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".s3g";

                map.Save(filename);

                if(!inFromException)
                    System.IO.File.Copy(filename, "./GameLog.s3g", true);
            }
        }

        private static void CreateTextureAtlasCache()
        {
        }

        private static void GenerateTextureAtlas()
        {
        }
    }
}
