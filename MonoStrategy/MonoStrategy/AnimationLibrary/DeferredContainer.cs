﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.IO;

namespace MonoStrategy
{
    internal class DeferredContainer
    {
        public UniqueMap<Int64, byte[]> Frames { get; private set; }

        public DeferredContainer()
        {
            Frames = new UniqueMap<Int64, byte[]>();
        }

        public void AddFrame(AnimationFrame inFrame)
        {
            if (inFrame.m_Bitmap == null)
                throw new ArgumentNullException("Given animation frame has no valid bitmap!");

            if(!Frames.ContainsKey(inFrame.Checksum))
                Frames.Add(inFrame.Checksum, inFrame.m_Bitmap);
        }

        public static DeferredContainer Load(Stream inSource)
        {
            DeferredContainer result = new DeferredContainer();
            BinaryReader reader = new BinaryReader(inSource);
            Int32 frameCount = reader.ReadInt32();

            for (int i = 0; i < frameCount; i++)
            {
                Int64 checksum = reader.ReadInt64();
                byte[] data = reader.ReadBytes(reader.ReadInt32());
                
                result.Frames.Add(checksum, data);
            }

            return result;
        }

        public void Store(Stream inTarget)
        {
            BinaryWriter writer = new BinaryWriter(inTarget);

            writer.Write((Int32)Frames.Count);

            foreach (KeyValuePair<Int64, byte[]> frame in Frames)
            {
                writer.Write((Int64)frame.Key);
                writer.Write((Int32)frame.Value.Length);
                writer.Write(frame.Value);
            }

            inTarget.Flush();
        }
    }
}
