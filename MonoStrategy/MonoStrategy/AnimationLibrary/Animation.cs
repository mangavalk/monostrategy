﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.IO;

namespace MonoStrategy
{
    public delegate void DDimensionChangedHandler();

    public class Animation
    {
        internal InternalBinding<AnimationFrame> m_Frames = new InternalBinding<AnimationFrame>();

        public AnimationFrame FirstFrame { get { return (Frames.Count == 0)?null: Frames[0]; } }
        public BindingList<AnimationFrame> Frames { get { return m_Frames; } }
        public String Name { get; internal set; }
        public String Path
        {
            get
            {
                if (SetOrNull != null)
                    return SetOrNull.Class.Library.Directory + "\\" + SetOrNull.Class.Name + "\\" + SetOrNull.Name + "\\" + Name;
                else
                    return Library.Directory + "\\" + Name;
            }
        }
        private Int32 m_OffsetX;
        private Int32 m_OffsetY;
        private Boolean m_IsFrozen;
        private AnimationFrame m_FrozenFrame;
        private Int32 m_SoundStartDelay;
        private Int32 m_SoundRepeatDelay;
        private Boolean m_PlaySound;
        private Boolean m_RepeatSound;
        private AudioObject m_Sound;

        public Int32 RenderIndex { get; set; }
        public AnimationSet SetOrNull { get; private set; }
        public AnimationLibrary Library { get; private set; }
        public Int32 SoundStartDelay { get { return m_SoundStartDelay; } set { ForceWriteable(); m_SoundStartDelay = value; } }
        public Int32 SoundRepeatDelay { get { return m_SoundRepeatDelay; } set { ForceWriteable(); m_SoundRepeatDelay = value; } }
        public Boolean PlaySound { get { return m_PlaySound; } set { ForceWriteable(); m_PlaySound = value; } }
        public Boolean RepeatSound { get { return m_RepeatSound; } set { ForceWriteable(); m_RepeatSound = value; } }
        public AudioObject Sound { get { return m_Sound; } set { ForceWriteable(); m_Sound = value; } }

        public Boolean IsFrozen { 
            get { return m_IsFrozen; }
            set
            {
                ForceWriteable();

                m_IsFrozen = value;
                if (value && (FrozenFrame == null) && (Frames.Count > 0))
                    FrozenFrame = Frames[0];
            }
        }
        public Int32 OffsetX { 
            get { return m_OffsetX; }
            set { ForceWriteable(); m_OffsetX = value; NotifyDimensionChange(); }
        }
        public Int32 OffsetY { 
            get { return m_OffsetY; }
            set { ForceWriteable(); m_OffsetY = value; NotifyDimensionChange(); }
        }
        public event DDimensionChangedHandler OnDimensionChanged;
        public Int32 Width { get; private set; }
        public Int32 Height { get; private set; }
        public Boolean IsVisible { get; set; }
        public AnimationFrame FrozenFrame
        {
            get { return m_FrozenFrame; }
            set
            {
                ForceWriteable();

                if (value != null)
                {
                    if (!Frames.Contains(value))
                        throw new ArgumentException("Given frozen frame is not contained in animation.");
                }

                m_FrozenFrame = value;
            }
        }


        internal void Save(BinaryWriter inWriter)
        {
            // write animation to stream
            inWriter.Write((Byte)4); // animation type ID
            inWriter.Write((UInt16)0x1000); // animation version

            inWriter.Write((String)Name);
            inWriter.Write((Int32)m_OffsetX);
            inWriter.Write((Int32)m_OffsetY);
            inWriter.Write((Int32)RenderIndex);
            inWriter.Write((Boolean)m_IsFrozen);
            inWriter.Write((Int32)SoundStartDelay);
            inWriter.Write((Int32)SoundRepeatDelay);
            inWriter.Write((Boolean)PlaySound);
            inWriter.Write((Boolean)RepeatSound);

            if (Sound != null)
                inWriter.Write((String)Sound.Name);
            else
                inWriter.Write((String)"");

            inWriter.Write((Int32)m_Frames.Count);

            foreach (var frame in m_Frames)
            {
                frame.Save(inWriter);
            }

            if (m_FrozenFrame != null)
                inWriter.Write((Int64)m_FrozenFrame.Checksum);
            else
                inWriter.Write((Int64)0);
        }


        internal static Animation Load(AnimationLibrary inLibrary, AnimationSet inSetOrNull, BinaryReader inReader)
        {
            Animation result;

            if (inReader.ReadByte() != 4)
                throw new InvalidDataException();

            switch (inReader.ReadUInt16())
            {
                case 0x1000:
                    {
                        if (inSetOrNull != null)
                            result = new Animation(inReader.ReadString(), inSetOrNull);
                        else
                            result = new Animation(inReader.ReadString(), inLibrary);

                        result.m_OffsetX = inReader.ReadInt32();
                        result.m_OffsetY = inReader.ReadInt32();
                        result.RenderIndex = inReader.ReadInt32();
                        result.m_IsFrozen = inReader.ReadBoolean();
                        result.SoundStartDelay = inReader.ReadInt32();
                        result.SoundRepeatDelay = inReader.ReadInt32();
                        result.PlaySound = inReader.ReadBoolean();
                        result.RepeatSound = inReader.ReadBoolean();

                        String sndName = inReader.ReadString();

                        if (!String.IsNullOrEmpty(sndName))
                            result.Sound = result.Library.FindAudio(sndName);

                        for (int i = 0, count = inReader.ReadInt32(); i < count; i++)
                        {
                            result.m_Frames.Add(AnimationFrame.Load(result, inReader));
                        }

                        Int64 frozenID = inReader.ReadInt64();

                        if (frozenID != 0)
                        {
                            foreach (var frame in result.m_Frames)
                            {
                                if (frame.Checksum == frozenID)
                                {
                                    result.m_FrozenFrame = frame;
                                    break;
                                }
                            }

                            if (result.m_FrozenFrame == null)
                                throw new InvalidDataException();
                        }

                    } break;
                default:
                    throw new InvalidDataException();
            }

            return result;
        }

        public void ComputeDimension()
        {
            ComputeDimension(true);
        }

        internal void NotifyDimensionChange()
        {
            if (OnDimensionChanged != null)
                OnDimensionChanged();
        }

        public void ComputeDimension(Boolean inNotifyParent)
        {
            ForceWriteable();

            Int32 newWidth = 0;
            Int32 newHeight = 0;

            foreach (var frame in Frames)
            {
                newWidth = Math.Max(newWidth, frame.OffsetX + frame.Width);
                newHeight = Math.Max(newHeight, frame.OffsetY + frame.Height);
            }

            Width = newWidth;
            Height = newHeight;

            if (inNotifyParent)
                NotifyDimensionChange();
        }

        internal Animation(String inName, AnimationSet inSet)
        {
            Name = inName;
            SetOrNull = inSet;
            Library = inSet.Class.Library;
        }

        internal Animation(String inName, AnimationLibrary inLibrary)
        {
            Name = inName;
            Library = inLibrary;
        }

        public AnimationFrame AddFrame()
        {
            ForceWriteable();

            AnimationFrame result = new AnimationFrame(this);

            m_Frames.AddInternal(result);

            if (IsFrozen && (FrozenFrame == null))
                FrozenFrame = result;

            result.Index = Library.FrameCount++;

            ComputeDimension();

            return result;
        }

        public void RemoveFrame(AnimationFrame inFrame)
        {
            ForceWriteable();

            if(!m_Frames.Contains(inFrame))
                throw new ArgumentException("Given frame is not contained in the current animation.");

            m_Frames.RemoveInternal(inFrame);

            if (m_FrozenFrame == inFrame)
            {
                if (Frames.Count > 0)
                    m_FrozenFrame = Frames[0];
                else
                    m_FrozenFrame = null;
            }

            ComputeDimension();
        }

        public void MoveFrameLeft(AnimationFrame inFrame)
        {
            ForceWriteable();

            int pos = m_Frames.IndexOf(inFrame);

            if (pos < 0)
                throw new ArgumentException("Given frame is not contained in the current animation.");

            if (pos == 0)
                return;

            m_Frames.RemoveAtInternal(pos);
            m_Frames.InsertInternal(pos - 1,inFrame);
        }

        public void MoveFrameRight(AnimationFrame inFrame)
        {
            ForceWriteable();

            int pos = m_Frames.IndexOf(inFrame);

            if (pos < 0)
                throw new ArgumentException("Given frame is not contained in the current animation.");

            if (pos == m_Frames.Count - 1)
                return;

            m_Frames.RemoveAtInternal(pos);
            m_Frames.InsertInternal(pos + 1, inFrame);
        }

        private void ForceWriteable()
        {
            Library.ForceWriteable();
        }
    }

  
}
