﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace MonoStrategy
{
    internal abstract class AbstractTerrainGenerator
    {
        protected CrossRandom m_Random;

        public TerrainDefinition Terrain { get { return Map.Terrain; } }
        public ResourceManager ResMgr { get { return Map.ResMgr; } }
        public GameMap Map { get; private set; }
        public Int32 Size { get { return Map.Size; } }
        public Int64 Seed { get; private set; }

        protected AbstractTerrainGenerator(GameMap inMap, long inSeed)
        {
            if (inMap == null)
                throw new ArgumentNullException();

            m_Random = new CrossRandom((int)inSeed);
            Map = inMap;
            Seed = inSeed;
        }
    }

    internal class RandomTerrainGenerator : AbstractTerrainGenerator
    {
        public static void Run(GameMap inMap, long inSeed)
        {
            new RandomTerrainGenerator(inMap, inSeed);
        }

        private RandomTerrainGenerator(GameMap inMap, long inSeed) : base(inMap, inSeed)
        {
            double[] heights = new double[Size * Size];
            double[] foilages = new double[Size * Size];
            double[] resources = new double[Size * Size];

            GenerateLayers((int)inSeed, Size, Size, heights, foilages, resources);

            for (int x = 0, offset = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++, offset++)
                {
                    double height = heights[offset];
                    double foilage = foilages[offset];

                    Terrain.SetHeightAt(x, y, (byte)(128 + 127 * height));

                    if ((foilage > -0.8) && (foilage < -0.5))
                    {
                        Terrain.SetFlagsAt(x, y, TerrainCellFlags.Tree_01);
                    }
                }
            }
        }

        private void GenerateLayers(
           int inSeed,
           int inWidth,
           int inHeight,
           double[] outHeightLayer,
           double[] outFoilageLayer,
           double[] outResourceLayer)
        {
            PerlinNoise heightSelector = new PerlinNoise(8, 0.018, 1.4, inSeed);
            PerlinNoise treeSelector = new PerlinNoise(16, 0.28, 1.1, inSeed + 100000);
            PerlinNoise foilageSelector = new PerlinNoise(1, 0.02, 1.0, inSeed + 10000);
            double minHeight = 1.0f, maxHeight = -1.0f;
            double minFoilage = 1.0f, maxFoilage = -1.0f;

            for (int y = 0, offset = 0; y < inHeight; y++)
            {
                for (int x = 0; x < inWidth; x++, offset++)
                {
                    double height = heightSelector.Perlin_noise_2D(x, y);
                    double foilage = treeSelector.Perlin_noise_2D(x, y) * foilageSelector.Perlin_noise_2D(x, y);

                    if (outHeightLayer != null)
                    {
                        outHeightLayer[offset] = height;

                        minHeight = Math.Min(minHeight, height);
                        maxHeight = Math.Max(maxHeight, height);
                    }

                    if (outFoilageLayer != null)
                    {
                        outFoilageLayer[offset] = foilage;

                        minFoilage = Math.Min(minFoilage, foilage);
                        maxFoilage = Math.Max(maxFoilage, foilage);
                    }
                }
            }

            const double maxDerivative = 0.05;

            for (int y = 0, offset = 0; y < inHeight; y++)
            {
                for (int x = 0; x < inWidth - 1; x++, offset++)
                {
                    double diff = outHeightLayer[offset + 1] - outHeightLayer[offset];

                    if (Math.Abs(diff) <= maxDerivative)
                        continue;

                    if (diff < 0)
                        outHeightLayer[offset + 1] = outHeightLayer[offset] - maxDerivative;
                    else
                        outHeightLayer[offset + 1] = outHeightLayer[offset] + maxDerivative;
                }

                offset++;
            }

            for (int x = 0; x < inWidth; x++)
            {
                for (int y = 0, offset = x; y < inHeight - 1; y++, offset += inWidth)
                {
                    double diff = outHeightLayer[offset + inWidth] - outHeightLayer[offset];

                    if (Math.Abs(diff) <= maxDerivative)
                        continue;

                    if (diff < 0)
                        outHeightLayer[offset + inWidth] = outHeightLayer[offset] - maxDerivative;
                    else
                        outHeightLayer[offset + inWidth] = outHeightLayer[offset] + maxDerivative;
                }
            }

            // normalize layers to [-1,1]
            for (int y = 0, offset = 0; y < inHeight; y++)
            {
                for (int x = 0; x < inWidth; x++, offset++)
                {
                    outHeightLayer[offset] = -1.0 + (outHeightLayer[offset] - minHeight) * 2 / (maxHeight - minHeight);
                    outFoilageLayer[offset] = -1.0 + (outFoilageLayer[offset] - minFoilage) * 2 / (maxFoilage - minFoilage);
                }
            }

        }
    }
}