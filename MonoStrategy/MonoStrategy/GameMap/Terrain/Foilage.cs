﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    public enum FoilageState
    {
        Growing,
        Grown,
        BeingCut,
    }

    public enum FoilageType
    {
        Tree1 = 1,
        //Tree2 = 2,
        //Tree3 = 4,
        //Tree4 = 8,
        //Tree = Tree1 | Tree2, // | Tree2 | Tree3 | Tree4,

        Grain = 128,
        Wine = 256,
        Honey = 512,
        Rice = 1024,
    }

    public delegate void DOnAddFoilage<TSender>(TSender inSender, Foilage inFoilage);
    public delegate void DOnRemoveFoilage<TSender>(TSender inSender, Foilage inFoilage);

    public class Foilage : PositionTracker
    {
        public FoilageState State { get; private set; }
        public FoilageType Type { get; private set; }
        public event Procedure<Foilage> OnStateChanged;

        internal Foilage(Point inPosition, FoilageType inType, FoilageState inState)
        {
            Type = inType;
            State = inState;
            Position = CyclePoint.FromGrid(inPosition);
        }

        private void RaiseStateChange()
        {
            if (OnStateChanged != null)
                OnStateChanged(this);
        }

        internal void MarkAsGrown()
        {
            if (State != FoilageState.Growing)
                throw new InvalidOperationException();

            State = FoilageState.Grown;

            RaiseStateChange();
        }

        internal void MarkAsBeingCut()
        {
            if (State != FoilageState.Grown)
                throw new InvalidOperationException();

            State = FoilageState.BeingCut;

            RaiseStateChange();
        }
    }

    public delegate void DOnAddStone<TSender>(TSender inSender, Stone inStone);
    public delegate void DOnRemoveStone<TSender>(TSender inSender, Stone inStone);

    public class Stone : PositionTracker
    {
        public int RemainingStones { get; private set; }
        public int ActualStones { get; private set; }
        public event Procedure<Stone> OnStateChanged;

        internal Stone(Point inPosition, int inInitialStones)
        {
            if (inInitialStones < 0)
                throw new ArgumentOutOfRangeException();

            ActualStones = RemainingStones = inInitialStones;
            Position = CyclePoint.FromGrid(inPosition);
        }

        private void RaiseStateChange()
        {
            if (OnStateChanged != null)
                OnStateChanged(this);
        }

        internal void MarkAsBeingCut()
        {
            if (RemainingStones <= 0)
                throw new InvalidOperationException();

            RemainingStones--;
        }

        internal void Cut()
        {
            if (ActualStones <= 0)
                throw new InvalidOperationException();

            ActualStones--;

            RaiseStateChange();
        }
    }
}
