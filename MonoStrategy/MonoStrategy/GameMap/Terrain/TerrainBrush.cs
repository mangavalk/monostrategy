﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class TerrainBrush
    {
        internal int[,] Values { get; private set; }
        internal int Width { get; private set; }
        internal int Height { get; private set; }
        internal int Variance { get; private set; }

        internal TerrainBrush(int inWidth, int inHeight)
        {
            Values = new int[inWidth, inHeight];
            Width = inWidth;
            Height = inHeight;
        }

        internal static TerrainBrush CreateSphereBrush(int inRadius, int inHeight)
        {
            TerrainBrush brush = new TerrainBrush(inRadius * 2, inRadius * 2);
            double step = Math.PI / (inRadius * 2 - 1);
            double dx = 0, dy = 0;

            for(int x = 0; x < inRadius * 2; x++)
            {
                for(int y = 0; y < inRadius * 2; y++)
                {
                    brush.Values[x, y] = (int)(Math.Sin(dy) * Math.Sin(dx) * inHeight);
                    brush.Variance += brush.Values[x, y]; 

                    dx += step;
                }
                dx = 0;
                dy += step;
            }

            return brush;
        }
    }
}
