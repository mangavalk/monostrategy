﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    public abstract class VisualUtilities
    {
        public static VisualUtilities Instance { get; set; }

        /// <summary>
        /// Schedules the given task for execution at the next rendering cycle. 
        /// </summary>
        public static void SynchronizeTask(Procedure inTask)
        {
            Instance.SynchronizeTaskImpl(inTask);
        }
        protected abstract void SynchronizeTaskImpl(Procedure inTask);

        public static void SynchronizeTask(long inDueTimeMillis, Procedure inTask)
        {
            Instance.SynchronizeTaskImpl(inDueTimeMillis, inTask);
        }
        protected abstract void SynchronizeTaskImpl(long inDueTimeMillis, Procedure inTask);

        public static void Hide(Movable inMovable)
        {
            Instance.HideImpl(inMovable);
        }
        protected abstract void HideImpl(Movable inMovable);

        public static void DirectedAnimation(
            Movable inMovable,
            String inClass,
            Direction? inDirectionOverride,
            bool inRestart,
            bool inRepeat)
        {
            Instance.DirectedAnimationImpl(inMovable, inClass, inDirectionOverride, inRestart, inRepeat);
        }
        protected abstract void DirectedAnimationImpl(
            Movable inMovable,
            String inClass,
            Direction? inDirectionOverride,
            bool inRestart,
            bool inRepeat);

        public static void Animate(
            Movable inMovable,
            String inClass,
            bool inRestart,
            bool inRepeat)
        {
            Instance.AnimateImpl(inMovable, inClass, inRestart, inRepeat);
        }
        protected abstract void AnimateImpl(
            Movable inMovable,
            String inClass,
            bool inRestart,
            bool inRepeat);

        public static void Animate(
            BaseBuilding inBuilding,
            String inClass)
        {
            Instance.AnimateImpl(inBuilding, inClass);
        }
        protected abstract void AnimateImpl(
            BaseBuilding inBuilding,
            String inClass);

        public static void AnimateAroundCenter(
            Point inCenter,
            String inAnimationClass,
            String inAnimationSet)
        {
            Instance.AnimateAroundCenterImpl(inCenter, inAnimationClass, inAnimationSet);
        }
        protected abstract void AnimateAroundCenterImpl(
            Point inCenter,
            String inAnimationClass,
            String inAnimationSet);

        public static int GetDurationMillis(Movable inMovable, String inAnimClass)
        {
            return Instance.GetDurationMillisImpl(inMovable, inAnimClass);
        }
        protected abstract int GetDurationMillisImpl(Movable inMovable, String inAnimClass);

        public static int GetDurationMillis(BaseBuilding inBuilding, String inAnimClass)
        {
            return Instance.GetDurationMillisImpl(inBuilding, inAnimClass);
        }
        protected abstract int GetDurationMillisImpl(BaseBuilding inBuilding, String inAnimClass);

        public static bool HasAnimation(Movable inMovable, String inAnimClass)
        {
            return Instance.HasAnimationImpl(inMovable, inAnimClass);
        }
        protected abstract bool HasAnimationImpl(Movable inMovable, String inAnimClass);

        public static bool HasAnimation(Foilage inFoilage, String inAnimClass)
        {
            return Instance.HasAnimationImpl(inFoilage, inAnimClass);
        }
        protected abstract bool HasAnimationImpl(Foilage inFoilage, String inAnimClass);

        public static bool HasAnimation(BaseBuilding inBuilding, String inAnimClass)
        {
            return Instance.HasAnimationImpl(inBuilding, inAnimClass);
        }
        protected abstract bool HasAnimationImpl(BaseBuilding inBuilding, String inAnimClass);

        public static int GetDurationMillis(Foilage inFoilage, String inState)
        {
            return Instance.GetDurationMillisImpl(inFoilage, inState);
        }
        protected abstract int GetDurationMillisImpl(Foilage inFoilage, String inState);

        public static void Animate(
            Foilage inFoilage,
            String inState,
            bool inRestart,
            bool inRepeat)
        {
            Instance.AnimateImpl(inFoilage, inState, inRestart, inRepeat);
        }
        protected abstract void AnimateImpl(
            Foilage inFoilage,
            String inState,
            bool inRestart,
            bool inRepeat);

        public static void Animate(Stone inStone)
        {
            Instance.AnimateImpl(inStone);
        }
        protected abstract void AnimateImpl(Stone inStone);

        public static void ShowMessage(String inMessage)
        {
            Instance.ShowMessageImpl(inMessage);
        }
        protected abstract void ShowMessageImpl(String inMessage);

        public static void ShowError(String inMessage)
        {
            Instance.ShowErrorImpl(inMessage);
        }
        protected abstract void ShowErrorImpl(String inMessage);
    }
}
