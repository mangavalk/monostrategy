﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    public enum WalkResult
    {
        Success,
        NotFound,
        Abort,
    }

    public static class GridSearch
    {
        /// <summary>
        /// Invokes the handler with values, walking along the whole grid in a rectangular
        /// spiral around start position. What exactly is the "grid" is within your power.
        /// </summary>
        /// <returns>
        /// Either WalkResult.Success or WalkResult.NotFound. If a handler returns WalkResult.Abort, then
        /// this method will return WalkResult.NotFound instead, but abort the search.
        /// </returns>
        /// <param name="inStartPos">An abstract point to start walking.</param>
        /// <param name="inWidth">The total width of the abstract grid.</param>
        /// <param name="inHeight">The total height of the abstract grid.</param>
        /// <param name="inHandler">A handler to be called for every encountered cell.</param>
        public static WalkResult GridWalkAround(Point inStartPos, Int32 inWidth, Int32 inHeight, Func<Point, WalkResult> inHandler)
        {
            Boolean hasField;
            Point center = inStartPos;
            Int32 radius = 1;

            if (inHandler(new Point(center.X, center.Y)) == WalkResult.Success)
                return WalkResult.Success;

            do
            {
                hasField = false;

                WalkResult res;
                Point start = new Point(
                    center.X + radius,
                    center.Y - radius);

                // walk down
                if ((center.X + radius >= 0) && (center.X + radius < inWidth))
                {
                    for (int y = center.Y - radius; y <= center.Y + radius; y++)
                    {
                        if (y < 0) continue;
                        if (y >= inHeight) break;

                        if ((res = inHandler(new Point(center.X + radius, y))) == WalkResult.Success)
                            return WalkResult.Success;

                        if (res == WalkResult.Abort)
                            return WalkResult.NotFound;

                        hasField = true;
                    }
                }

                // walk left
                if ((center.Y + radius >= 0) && (center.Y + radius < inHeight))
                {
                    for (int x = center.X + radius - 1; x >= center.X - radius; x--)
                    {
                        if (x < 0) break;
                        if (x >= inWidth) continue;

                        if ((res = inHandler(new Point(x, center.Y + radius))) == WalkResult.Success)
                            return WalkResult.Success;

                        if (res == WalkResult.Abort)
                            return WalkResult.NotFound;

                        hasField = true;
                    }
                }

                // walk up
                if ((center.X - radius >= 0) && (center.X - radius < inWidth))
                {
                    for (int y = center.Y + radius - 1; y >= center.Y - radius; y--)
                    {
                        if (y < 0) break;
                        if (y >= inHeight) continue;

                        if ((res = inHandler(new Point(center.X - radius, y))) == WalkResult.Success)
                            return WalkResult.Success;

                        if (res == WalkResult.Abort)
                            return WalkResult.NotFound;

                        hasField = true;
                    }
                }

                // walk right
                if ((center.Y - radius >= 0) && (center.Y - radius < inHeight))
                {
                    for (int x = center.X - radius + 1; x <= center.X + radius - 1; x++)
                    {
                        if (x < 0) continue;
                        if (x >= inWidth) break;

                        if ((res = inHandler(new Point(x, center.Y - radius))) == WalkResult.Success)
                            return WalkResult.Success;

                        if (res == WalkResult.Abort)
                            return WalkResult.NotFound;

                        hasField = true;
                    }
                }

                radius++;
            } while (hasField);

            return WalkResult.NotFound; // was not aborted by handler
        }

        /// <summary>
        /// Circles around the given center while maintaining a constant distance of <paramref name="inRadius"/>
        /// to the center and a distance of <paramref name="inStepWidth"/> to the previous call of the handler,
        /// if possible. In contrast to <see cref="WalkAround"/>, this method does NOT cover an area but only a
        /// closed line (approx. a circle). 
        /// </summary>
        /// <param name="inCenter"></param>
        /// <param name="inRadius"></param>
        /// <param name="inStepWidth"></param>
        /// <param name="inHandler"></param>
        public static void GridCircleAround(Point inCenter, int inGridWidth, int inGridHeight, int inRadius, int inStepWidth, Procedure<Point> inHandler)
        {
            Rectangle bounds = new Rectangle(
                inCenter.X - inRadius,
                inCenter.Y - inRadius,
                inRadius * 2,
                inRadius * 2);
            Point prevPoint = new Point(Int32.MinValue, Int32.MinValue);

            for (int y = bounds.Top; y < bounds.Bottom; y++)
            {
                CircleStep(new Point(bounds.Left, y), prevPoint, inCenter, inGridWidth, inGridHeight, inRadius, inStepWidth, (pos) =>
                {
                    prevPoint = pos;
                    inHandler(pos);
                });
            }

            for (int x = bounds.Left; x < bounds.Right; x++)
            {
                CircleStep(new Point(x, bounds.Bottom), prevPoint, inCenter, inGridWidth, inGridHeight, inRadius, inStepWidth, (pos) =>
                {
                    prevPoint = pos;
                    inHandler(pos);
                });
            }

            for (int y = bounds.Bottom - 1; y >= bounds.Top; y--)
            {
                CircleStep(new Point(bounds.Right, y), prevPoint, inCenter, inGridWidth, inGridHeight, inRadius, inStepWidth, (pos) =>
                {
                    prevPoint = pos;
                    inHandler(pos);
                });
            }

            for (int x = bounds.Right - 1; x >= bounds.Left; x--)
            {
                CircleStep(new Point(x, bounds.Top), prevPoint, inCenter, inGridWidth, inGridHeight, inRadius, inStepWidth, (pos) =>
                {
                    prevPoint = pos;
                    inHandler(pos);
                });
            }
        }

        private static bool CircleStep(Point inPoint, Point inPrevPoint, Point inCenter, int inGridWidth, int inGridHeight, int inRadius, int inStepWidth, Procedure<Point> inHandler)
        {
            // walk from point to center until distance is smaller or equal to radius
            double xStep = inCenter.X - inPoint.X;
            double yStep = inCenter.Y - inPoint.Y;
            double norm = Math.Sqrt(xStep * xStep + yStep * yStep);
            bool xFlip = inCenter.X > inPoint.X;
            bool yFlip = inCenter.Y > inPoint.Y;
            Point last = inPoint;
            bool xOnce = inCenter.X - inPoint.X == 0;
            bool yOnce = inCenter.Y - inPoint.Y == 0;

            xStep /= norm;
            yStep /= norm;


            for (double x = inPoint.X; (x < inCenter.X) == xFlip; x += xStep, xFlip = xOnce?!xFlip:xFlip)
            {
                for (double y = inPoint.Y; (y < inCenter.Y) == yFlip; y += yStep, yFlip = yOnce ? !yFlip : yFlip)
                {
                    Point current = new Point((int)x, (int)y);

                    if ((current == last) || (current.DistanceTo(inCenter) > inRadius))
                        continue;

                    if (current.DistanceTo(inPrevPoint) < inStepWidth)
                        return false;

                    inHandler(current);

                    return true;
                }
            }

            return false;
        }
    }
}
