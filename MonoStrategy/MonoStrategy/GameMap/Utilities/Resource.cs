﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{

    [Serializable]
    public enum Resource : int
    {
        Axe,
        Bow,
        Bread,
        Coal,
        Corn,
        Fish,
        Gold,
        GoldOre,
        Grain,
        Hammer,
        Hook,
        Iron,
        IronOre,
        Meat,
        PickAxe,
        Saw,
        Scythe,
        Sheep,
        Shovel,
        Spear,
        Sword,
        Stone,
        Timber,
        Water,
        Wine,
        Wood,

        // must be the last item!
        Max,
    }
}
