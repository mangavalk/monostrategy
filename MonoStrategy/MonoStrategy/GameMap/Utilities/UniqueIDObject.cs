﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MonoStrategy
{
    /// <summary>
    /// Since the economy is fully deterministic, it is a very handy idea to ultimately derive some important
    /// objects like jobs, movables and the like from DebugObject, providing unique IDs and debug methods
    /// for easy bug tracking.
    /// </summary>
    internal sealed class UniqueIDObject
    {
        private static readonly SortedDictionary<long, WeakReference> m_IDToObject = new SortedDictionary<long, WeakReference>();
        private static long m_ObjectIDCounter = 0;
        private static Timer m_CleanupTimer = new Timer(OnCleanupTimer, null, 10000, 10000);
        
        internal long UniqueID { get; private set; }
        internal Object Reference { get; private set; }

        private static void OnCleanupTimer(Object unused)
        {
            lock (m_IDToObject)
            {
                Stack<Int64> removals = new Stack<long>();

                foreach (var entry in m_IDToObject)
                {
                    if (!entry.Value.IsAlive)
                        removals.Push(entry.Key);
                }

                foreach (var id in removals)
                {
                    m_IDToObject.Remove(id);
                }
            }
        }

        internal UniqueIDObject(Object inReference)
        {
            if (inReference == null)
                throw new ArgumentNullException();

            lock (m_IDToObject)
            {
                UniqueID = ++m_ObjectIDCounter;
                Reference = inReference;

                m_IDToObject.Add(UniqueID, new WeakReference(this));
            }
        }

        internal void DebugBreakOnID(long inObjectID)
        {
            if (UniqueID == inObjectID)
                System.Diagnostics.Debugger.Break();
        }

        internal static Object Resolve(Int64 inUniqueID)
        {
            lock (m_IDToObject)
            {
                return ((UniqueIDObject)m_IDToObject[inUniqueID].Target).Reference;
            }
        }
    }
}
