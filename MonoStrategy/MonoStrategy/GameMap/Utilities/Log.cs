﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.IO;
using System.Diagnostics;

namespace MonoStrategy
{
    internal static class Debug
    {
        internal static void Assert(bool inExpression)
        {
            Assert(inExpression, "Debug assertion failed.");
        }

        internal static void Assert(bool inExpression, String inMessage, params Object[] inParams)
        {
#if DEBUG
            if(!inExpression)
                throw new ApplicationException(String.Format(inMessage, inParams));
#endif
        }
    }

    public static class Log
    {
        private static readonly Object m_Lock = new Object();

        public static event Procedure<Exception> OnCriticalException;

        static Log()
        {
            File.Delete("./log.txt");
        }

        public static void LogExceptionModal(Exception e)
        {
            lock (m_Lock)
            {
                LogRaw("### ERROR ==> ", e.ToString());

                VisualUtilities.ShowError(e.ToString());
            }
        }

        public static void LogExceptionCritical(Exception e)
        {
            LogExceptionModal(e);

            try
            {
                if(OnCriticalException != null)
                    OnCriticalException(e);
            }
            catch
            {
            }

            // give a chance to look at application state
            System.Diagnostics.Debugger.Break();

            Exit();
        }

        public static void LogException(Exception e)
        {
            LogRaw("### ERROR ==> ", e.ToString());
        }

        public static void LogMessage(String inMessage)
        {
            LogRaw("# INFO ", inMessage);
        }

        public static void LogMessageModal(String inMessage)
        {
            LogRaw("# INFO ", inMessage);

            VisualUtilities.ShowMessage(inMessage);
        }

        private static void LogRaw(String inPrefix, String inMessage)
        {
            lock (m_Lock)
            {
                String message = inPrefix + "[" + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + "]: " + inMessage + "\r\n";

                StreamWriter Writer = File.AppendText("./log.txt");

                using (Writer)
                {
                    Writer.Write(message);

                    Writer.Flush();
                }
            }
        }

        public static void Exit()
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}
