﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace MonoStrategy
{
    public class SettlerBuilding : BaseBuilding
    {
        private int m_SettlerCount = 0;
        private CyclePoint m_SpawnPoint;

        internal SettlerBuilding(BuildingManager inParent, BuildingConfig inConfig, Point inPosition)
            : base(inParent, inConfig, inPosition)
        {
            var pos = inConfig.ResourceStacks[0].Position;

            m_SettlerCount = inConfig.SettlerCount;
            m_SpawnPoint = CyclePoint.FromGrid(pos.X + Position.X, pos.Y + Position.Y);
        }

        internal override bool Update()
        {
            // already producing?
            long currentTime = Parent.MovMgr.CurrentCycle;
            long timeOffset = (currentTime - ProductionStart);

            if (timeOffset >= ProductionTime)
            {
                if (m_SettlerCount > 0)
                {
                    m_SettlerCount--;

                    // spawn settler
                    Parent.MovMgr.AddMovable(m_SpawnPoint, MovableType.Settler);
                }

                ProductionStart = currentTime;
            }

            return false;
        }
    }
}
