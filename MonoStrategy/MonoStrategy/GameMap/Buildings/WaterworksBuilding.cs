﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    public class WaterworksBuilding : OutputOnlyBuilding
    {
        internal WaterworksBuilding(BuildingManager inParent, BuildingConfig inConfig, Point inPosition)
            : base(inParent, inConfig, inPosition)
        {
            WorkingRadius = Int32.MaxValue;
        }

        protected override bool Plant(Procedure onCompleted)
        {
            return false;
        }

        protected override bool Produce(Procedure<bool> onCompleted)
        {
            Point waterspot;

            if (!Parent.Terrain.FindWater(SpawnPoint.Value.ToPoint(), 2, out waterspot))
                return false;

            // collect water
            JobCollectingWater job = new JobCollectingWater(SpawnWorker(), waterspot, this);

            job.OnCompleted += (unused, succeeded) => { onCompleted(succeeded); };
            WorkerOrNull.Job = job;

            return true;
        }
    }
}
