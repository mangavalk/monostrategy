﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    public abstract class OutputOnlyBuilding : WorkerBuilding
    {
        public int WorkingRadius { get; internal set; }

        internal Movable WorkerOrNull { get; set; }
        internal bool IsPlanting { get; private set; }
        internal bool IsProducing { get; private set; }
        internal bool IsBusy { get { return IsPlanting || IsProducing; } }

        internal OutputOnlyBuilding(BuildingManager inParent, BuildingConfig inConfig, Point inPosition)
            : base(inParent, inConfig, inPosition)
        {
            int provCount = inConfig.ResourceStacks.Count(e => (e.Type != Resource.Max) && (e.Direction == ResStackType.Provider));
            int queryCount = inConfig.ResourceStacks.Count(e => (e.Type != Resource.Max) && (e.Direction == ResStackType.Query));

            if (queryCount > 0)
                throw new ArgumentException("OutputOnlyBuilding can not have resource queries!");

            if (provCount != 1)
                throw new ArgumentException("OutputOnlyBuilding shall have exactly one resource provider!");
        }

        protected abstract bool Produce(Procedure<bool> onCompleted);
        protected abstract bool Plant(Procedure onCompleted);

        /// <summary>
        /// Is not to be called every cycle, since it is an expensive function.
        /// </summary>
        internal override bool Update()
        {
            if (!base.Update())
                return false;

            if (IsBusy)
                return false;

            if (Parent.MovMgr.CurrentCycle - ProductionStart < Config.ProductionTimeMillis)
                return false;

            // ### START NEW PRODUCTION CYCLE

            if (!Plant(OnPlantingCompleted))
            {
                // has space for outcomes?
                if (!HasFreeProvider())
                    return false;

                if (!Produce(OnProductionCompleted))
                    return false;

                IsProducing = true;
            }
            else
            {
                IsPlanting = true;
            }

            // the derived class is now responsible for completing the task and raise our handler...

            return true;
        }

        protected void DisposeWorker()
        {
            // dispose worker
            VisualUtilities.Hide(WorkerOrNull);
            Parent.MovMgr.MarkMovableForRemoval(WorkerOrNull);
            WorkerOrNull = null;
        }

        private void OnProductionCompleted(bool succeeded)
        {
            ProductionStart = Parent.MovMgr.CurrentCycle;
            IsProducing = false;

            DisposeWorker();

            if (succeeded)
                Providers[0].AddResource();
        }

        private void OnPlantingCompleted()
        {
            ProductionStart = Parent.MovMgr.CurrentCycle;
            IsPlanting = false;

            DisposeWorker();
        }

        protected Movable SpawnWorker()
        {
            if (WorkerOrNull != null)
                throw new InvalidOperationException();

            WorkerOrNull = Parent.MovMgr.AddMovable(SpawnPoint.Value, MovableType.Settler);

            return WorkerOrNull;
        }
    }
}
