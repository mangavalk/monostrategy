﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace MonoStrategy
{
    internal partial class TerrainRouting
    {
        private class CameFromMap
        {
            private SortedDictionary<PathNodeKey, PathNodeKey> m_Entries =
                new SortedDictionary<PathNodeKey, PathNodeKey>(PathNodeKey.Comparer);

            internal void Add(PathNodeKey refFrom, PathNodeKey refTo)
            {
                m_Entries.Add(refFrom, refTo);
            }

            internal PathNodeKey this[PathNodeKey key] { get { return m_Entries[key]; } }

            internal Int32 Count { get { return m_Entries.Count; } }

            internal void Clear()
            {
                m_Entries.Clear();
            }

            internal void GetPath(PathNodeKey inLastNode, LinkedList<MovablePathNode> outResult)
            {
                PathNodeKey item;

                if (m_Entries.TryGetValue(inLastNode, out item))
                    GetPath(item, outResult);

                outResult.AddLast(new MovablePathNode()
                {
                    Position = new Point(inLastNode.X, inLastNode.Y),
                    Time = inLastNode.Time,
                });
            }
        }
    }
}
