﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General internal License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General internal License for more details.
 *
 *  You should have received a copy of the GNU Affero General internal License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://opensettlers.codeplex.com/
 */
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [XmlRoot("TerrainConfig")]
    [Serializable]
    public class XMLTerrainConfig
    {
        [Serializable]
        public class XMLLevel
        {
            [XmlAttribute]
            public String Id { get; set; }
            [XmlAttribute]
            public Double TextureScale { get; set; }
            [XmlIgnore]
            public Double Margin { get; set; }
            [XmlAttribute]
            public Double RedNoiseDivisor { get; set; }
            [XmlAttribute]
            public Double BlueNoiseDivisor { get; set; }
            [XmlAttribute]
            public Double GreenNoiseDivisor { get; set; }

            public XMLLevel()
            {
                BlueNoiseDivisor = 12;
                RedNoiseDivisor = 12;
                GreenNoiseDivisor = 12;
                TextureScale = 32;
            }
        }

        [Serializable]
        public class XMLWater
        {
            [XmlAttribute]
            public Double Height { get; set; }
            [XmlAttribute]
            public Double Speed { get; set; }
            [XmlAttribute]
            public Double Amplitude { get; set; }
        }

        [XmlAttribute]
        public Double HeightScale { get; set; }
        [XmlAttribute]
        public Double NormalZScale { get; set; }
        [XmlAttribute]
        public Double RedNoiseFrequency { get; set; }
        [XmlAttribute]
        public Double BlueNoiseFrequency { get; set; }
        [XmlAttribute]
        public Double GreenNoiseFrequency { get; set; }
        [XmlElement]
        public XMLWater Water { get; set; }

        [XmlElement("LevelList"), XmlArrayItem("Level")]
        public List<XMLLevel> Levels { get; set; }

        public XMLTerrainConfig()
        {
            RedNoiseFrequency = 20;
            GreenNoiseFrequency = 20;
            BlueNoiseFrequency = 12;
            HeightScale = 8;
            NormalZScale = 3;

            Water = new XMLWater()
            {
                Speed = 500,
                Height = -0.3,
                Amplitude = 0.5,
            };

            Levels = new List<XMLLevel>();

            Levels.Add(new XMLLevel()
            {
                Id = "Level_00",
                Margin = -1.0,
            });

            Levels.Add(new XMLLevel()
            {
                Id = "Level_01",
                Margin = -0.66,
            });

            Levels.Add(new XMLLevel()
            {
                Id = "Level_02",
                TextureScale = 64,
                Margin = -0.33,
            });

            Levels.Add(new XMLLevel()
            {
                Id = "Level_03",
                Margin = 0,
            });

            Levels.Add(new XMLLevel()
            {
                Id = "Level_04",
                Margin = 0.33,
            });

            Levels.Add(new XMLLevel()
            {
                Id = "Level_05",
                Margin = 0.66,
            });
        }
    }
}
