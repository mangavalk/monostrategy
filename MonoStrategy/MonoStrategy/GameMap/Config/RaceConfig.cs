﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [Serializable]
    [DataContract]
    public enum ResStackType
    {
        /// <summary>
        /// A query is provided with resources.
        /// </summary>
        [EnumMember]
        Query,
        /// <summary>
        /// A provider serves resource queries.
        /// </summary>
        [EnumMember]
        Provider,
    }

    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [Serializable, DataContract]
    public enum BuildingWorkerType
    {
        [EnumMember]
        None,
        [EnumMember]
        Settler,
        [EnumMember]
        Axe,
        [EnumMember]
        Hammer,
        [EnumMember]
        Hook,
        [EnumMember]
        PickAxe,
        [EnumMember]
        Saw,
        [EnumMember]
        Scythe,
    }

    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [Serializable, DataContract]
    public class ResourceStackConfig
    {
        [XmlIgnore, DataMember]
        public Point Position { get; set; }
        [XmlAttribute, DataMember]
        public Resource Type { get; set; }
        [XmlAttribute, DataMember]
        public Int32 TimeOffset { get; set; }
        [XmlAttribute, DataMember]
        public Int32 CycleCount { get; set; }
        [XmlAttribute, DataMember]
        public Int32 MaxStackSize { get; set; }
        [XmlAttribute, DataMember]
        public Int32 QualityIndex { get; set; }
        [XmlAttribute, DataMember]
        public ResStackType Direction { get; set; }
    }

    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [Serializable, DataContract]
    public enum BuildingClass
    {
        [EnumMember]
        AnglerBuilding,
        [EnumMember]
        InputOutputBuilding,
        [EnumMember]
        MarketBuilding,
        [EnumMember]
        MineBuilding,
        [EnumMember]
        PlantProduceBuilding,
        [EnumMember]
        SettlerBuilding,
        [EnumMember]
        StoneCutterBuilding,
        [EnumMember]
        ToolSmithBuilding,
        [EnumMember]
        WaterworksBuilding,
        [EnumMember]
        BarracksBuilding,
    }

    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [Serializable, DataContract]
    public class BuildingConfig
    {
        [XmlAttribute, DataMember]
        public Int32 TypeIndex { get; set; }
        [XmlAttribute, DataMember]
        public Int32 SettlerCount { get; set; }
        [XmlAttribute, DataMember]
        public String Name { get; set; }
        [XmlAttribute, DataMember]
        public String AnimationClass { get; set; }
        [XmlAttribute, DataMember]
        public Boolean NoProductionRange { get; set; }
        public List<ResourceStackConfig> ResourceStacks { get; set; }
        [XmlAttribute, DataMember]
        public Int32 DamageResistance { get; set; }
        [XmlAttribute, DataMember]
        public Int32 WoodCount { get; set; }
        [XmlAttribute, DataMember]
        public Int32 StoneCount { get; set; }
        [XmlAttribute, DataMember]
        public Int32 ProductionTimeMillis { get; set; }
        [XmlAttribute("Class"), DataMember]
        public String ClassString { get; set; }
        [XmlIgnore]
        public Type ClassType { get; set; }
        [XmlIgnore, DataMember]
        public BuildingClass Class { get; set; }
        [XmlAttribute, DataMember]
        public String ClassParameter { get; set; }
        [XmlAttribute, DataMember]
        public int TabIndex { get; set; }
        [XmlAttribute, DataMember]
        public BuildingWorkerType Worker { get; set; }
        [XmlIgnore]
        public Resource WorkerTool
        {
            get
            {
                switch (Worker)
                {
                    case BuildingWorkerType.None: 
                    case BuildingWorkerType.Settler: return Resource.Max;
                    case BuildingWorkerType.Axe: return Resource.Axe;
                    case BuildingWorkerType.Hammer: return Resource.Hammer;
                    case BuildingWorkerType.Hook: return Resource.Hook;
                    case BuildingWorkerType.PickAxe: return Resource.PickAxe;
                    case BuildingWorkerType.Saw: return Resource.Saw;
                    case BuildingWorkerType.Scythe: return Resource.Scythe;
                    default:
                        throw new ArgumentException();
                }
            }
        }

        // non-serialized fields (computed by race config loader)
        [XmlIgnore]
        public List<Rectangle> GroundPlane { get; set; }
        [XmlIgnore]
        public List<Rectangle> ReservedPlane { get; set; }
        [XmlIgnore, DataMember]
        public Int32 GridWidth { get; set; }
        [XmlIgnore, DataMember]
        public Int32 GridHeight { get; set; }
        [XmlIgnore, DataMember]
        public Point StoneSpot { get; set; }
        [XmlIgnore, DataMember]
        public Point TimberSpot { get; set; }
        [XmlIgnore, DataMember]
        public bool[][] GroundGrid { get; set; }
        [XmlIgnore, DataMember]
        public bool[][] ReservedGrid { get; set; }
        [XmlIgnore, DataMember]
        public List<Point> ConstructorSpots { get; set; }
        [XmlIgnore, DataMember]
        public Int32 ProductionAnimTime { get; set; }

        internal BaseBuilding CreateInstance(BuildingManager inParent, Point inPosition)
        {
            ConstructorInfo constructor = ClassType.GetConstructor(
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, 
                null, new Type[] { typeof(BuildingManager), typeof(BuildingConfig), typeof(Point) }, null);
            ConstructorInfo constructorOpt = ClassType.GetConstructor(
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, 
                null, new Type[] { typeof(BuildingManager), typeof(BuildingConfig), typeof(Point), typeof(String) }, null);

            if(constructorOpt != null)
                return (BaseBuilding)constructorOpt.Invoke(new object[] { inParent, this, inPosition, ClassParameter });
            else
                return (BaseBuilding)constructor.Invoke(new object[] { inParent, this, inPosition });
        }

        public BuildingConfig()
        {
            ResourceStacks = new List<ResourceStackConfig>();
        }
    }

    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [XmlRoot("RaceConfig"), DataContract]
    public partial class RaceConfig
    {
        [DataMember]
        public List<BuildingConfig> Buildables { get; set; }
        [XmlAttribute, DataMember]
        public String Name { get; set; }
    }
}
