﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class JobFoilageCutting : OneTimeJob
    {
        internal PlantProduceBuilding Building { get; private set; }
        internal Foilage Foilage { get; private set; }
        internal Movable Worker { get; private set; }

        internal JobFoilageCutting(Movable inWorker, Foilage inFoilage, PlantProduceBuilding inBuilding) : base(inWorker)
        {
            if ((inWorker == null) || (inFoilage == null) || (inBuilding == null))
                throw new ArgumentNullException();

            Building = inBuilding;
            Foilage = inFoilage;
            Worker = inWorker;
            AnimationClass = Building.Config.AnimationClass + "ProduceWalking";

            Foilage.MarkAsBeingCut();
        }

        internal override bool Prepare()
        {
            if (!base.Prepare())
                return false;

            Point cutTreePos = new Point(Foilage.Position.XGrid + 1, Foilage.Position.YGrid + 1);
            String buildingPrefix = Building.Config.AnimationClass;

            AddAnimationStep(cutTreePos, () =>
                {
                    // walk to foilage
                    VisualUtilities.Animate(Worker, buildingPrefix + "ProduceWalking", inRestart: false, inRepeat: true);

                    return true;
                }, null);

            int timeCutting = VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "ProduceTask");
            int timeFalling = 0;

            if (VisualUtilities.HasAnimation(Foilage, "Falling"))
            {
                AddAnimationStepWithPathFollow(
                    timeCutting,
                    () =>
                    {
                        // run cutting animation
                        VisualUtilities.Animate(Worker, buildingPrefix + "ProduceTask", inRestart: true, inRepeat: true);

                        return true;
                    }, null);

                timeFalling = VisualUtilities.GetDurationMillis(Foilage, "Falling");

                AddAnimationStepWithPathFollow(
                    timeFalling,
                    () =>
                    {
                        // continue cutting animation but also animate falling foilage
                        VisualUtilities.Animate(Foilage, "Falling", inRestart: true, inRepeat: false);

                        return true;
                    }, null);
            }

            if (VisualUtilities.HasAnimation(Foilage, "Fractioning"))
            {
                AddAnimationStepWithPathFollow(
                    timeCutting - timeFalling, // only the first few frames of fractioning are coupled with cutting 
                    () =>
                    {
                        // continue cutting animation but also animate the foilage being cut
                        VisualUtilities.Animate(Foilage, "Fractioning", inRestart: true, inRepeat: false);
                        VisualUtilities.Animate(Worker, buildingPrefix + "ProduceTask", inRestart: false, inRepeat: true);

                        // register foilage for removal
                        Building.Parent.QueueWorkItem(
                            VisualUtilities.GetDurationMillis(Foilage, "Fractioning"),
                            () => { Building.Parent.ResMgr.RemoveFoilage(Foilage); });

                        return true;
                    }, null);
            }

            if (VisualUtilities.HasAnimation(Worker, buildingPrefix + "Pickup"))
            {
                AddAnimationStepWithPathFollow(
                    VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "Pickup"),
                    () =>
                    {
                        // continue animating the tree... additionally we now pickup the cut foilage
                        VisualUtilities.Animate(Worker, buildingPrefix + "Pickup", inRestart: true, inRepeat: false);

                        return true;
                    }, null);
            }

            // walk back to building
            AddAnimationStep(Building.SpawnPoint.Value.ToPoint(),
                () =>
                {
                    // switch to carrying animation
                    VisualUtilities.Animate(Worker, buildingPrefix + "Carrying", inRestart: true, inRepeat: true);

                    if (!VisualUtilities.HasAnimation(Foilage, "Fractioning"))
                    {
                        Building.Parent.ResMgr.RemoveFoilage(Foilage);
                    }

                    return true;
                },
                (succeeded) =>
                {
                    if (!VisualUtilities.HasAnimation(Worker, buildingPrefix + "Drop"))
                        RaiseCompletion(succeeded);

                    return true;
                });

            if (VisualUtilities.HasAnimation(Worker, buildingPrefix + "Drop"))
            {
                AddAnimationStepWithPathFollow(
                    VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "Drop"),
                    () =>
                    {
                        // animate dropping the cut foilage
                        VisualUtilities.Animate(Worker, buildingPrefix + "Drop", inRestart: true, inRepeat: false);

                        return true;
                    },
                    (succeeded) =>
                    {
                        RaiseCompletion(succeeded);

                        return true;
                    });
            }

            return true;
        }
    }
}
