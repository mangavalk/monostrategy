﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class JobCollectingWater : OneTimeJob
    {
        internal WaterworksBuilding Building { get; private set; }
        internal Point WaterSpot { get; private set; }
        internal Movable Worker { get; private set; }

        internal JobCollectingWater(Movable inWorker, Point inWaterSpot, WaterworksBuilding inBuilding)
            : base(inWorker)
        {
            if ((inWorker == null) || (inBuilding == null))
                throw new ArgumentNullException();

            Building = inBuilding;
            WaterSpot = inWaterSpot;
            Worker = inWorker;
            AnimationClass = Building.Config.AnimationClass + "Walking";
        }

        internal override bool Prepare()
        {
            if (!base.Prepare())
                return false;

            String buildingPrefix = Building.Config.AnimationClass;

            AddAnimationStep(WaterSpot, () =>
            {
                // walk to collecting position
                VisualUtilities.Animate(Worker, buildingPrefix + "Walking", inRestart: false, inRepeat: true);

                return true;
            }, null);

            AddAnimationStepWithPathFollow(
                VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "Working"),
                () =>
                {
                    // run plating animation
                    VisualUtilities.Animate(Worker, buildingPrefix + "Working", inRestart: true, inRepeat: false);

                    return true;
                },
                null);

            // walk back to building
            AddAnimationStep(Building.SpawnPoint.Value.ToPoint(),
                () =>
                {
                    // switch to carrying animation
                    VisualUtilities.Animate(Worker, buildingPrefix + "Carrying", inRestart: true, inRepeat: true);

                    return true;
                },
                (succeeded) =>
                {
                    RaiseCompletion(succeeded);

                    return true;
                });

            return true;
        }
    }
}
