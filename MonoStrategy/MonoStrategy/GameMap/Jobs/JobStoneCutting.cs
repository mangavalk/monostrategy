﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class JobStoneCutting : OneTimeJob
    {
        internal StoneCutterBuilding Building { get; private set; }
        internal Stone Stone { get; private set; }
        internal Movable Worker { get; private set; }

        internal JobStoneCutting(Movable inWorker, Stone inStone, StoneCutterBuilding inBuilding)
            : base(inWorker)
        {
            if ((inWorker == null) || (inStone == null) || (inBuilding == null))
                throw new ArgumentNullException();

            Building = inBuilding;
            Stone = inStone;
            Worker = inWorker;
            AnimationClass = Building.Config.AnimationClass + "Walking";

            Stone.MarkAsBeingCut();
        }

        internal override bool Prepare()
        {
            if (!base.Prepare())
                return false;

            Point cutStonePos = new Point(Stone.Position.XGrid + 1, Stone.Position.YGrid + 1);
            String buildingPrefix = Building.Config.AnimationClass;

            AddAnimationStep(cutStonePos, () =>
                {
                    // walk to stone
                    VisualUtilities.Animate(Worker, buildingPrefix + "Walking", inRestart: false, inRepeat: true);

                    return true;
                }, null);

            int timeCutting = VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "Working");

            AddAnimationStepWithPathFollow(
                timeCutting,
                () =>
                {
                    // run cutting animation
                    VisualUtilities.Animate(Worker, buildingPrefix + "Working", inRestart: true, inRepeat: true);

                    return true;
                },
                (succeeded) =>
                {
                    Stone.Cut();

                    if (Stone.ActualStones <= 0)
                    {
                        // mark stone for removal
                        MovMgr.QueueWorkItem(30000,
                            () =>
                            {
                                Building.Parent.ResMgr.RemoveStone(Stone);
                            });
                    }

                    return true;
                });

            if (VisualUtilities.HasAnimation(Worker, buildingPrefix + "Pickup"))
            {
                AddAnimationStepWithPathFollow(
                    VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "Pickup"),
                    () =>
                    {
                        VisualUtilities.Animate(Worker, buildingPrefix + "Pickup", inRestart: true, inRepeat: false);

                        return true;
                    }, null);
            }

            // walk back to building
            AddAnimationStep(Building.SpawnPoint.Value.ToPoint(),
                () =>
                {
                    // switch to carrying animation
                    VisualUtilities.Animate(Worker, buildingPrefix + "Carrying", inRestart: true, inRepeat: true);

                    return true;
                },
                (succeeded) =>
                {
                    if (!VisualUtilities.HasAnimation(Worker, buildingPrefix + "Drop"))
                        RaiseCompletion(succeeded);

                    return true;
                });

            if (VisualUtilities.HasAnimation(Worker, buildingPrefix + "Drop"))
            {
                AddAnimationStepWithPathFollow(
                    VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "Drop"),
                    () =>
                    {
                        // animate dropping the cut foilage
                        VisualUtilities.Animate(Worker, buildingPrefix + "Drop", inRestart: true, inRepeat: false);

                        return true;
                    },
                    (succeeded) =>
                    {
                        RaiseCompletion(succeeded);

                        return true;
                    });
            }

            return true;
        }
    }
}
