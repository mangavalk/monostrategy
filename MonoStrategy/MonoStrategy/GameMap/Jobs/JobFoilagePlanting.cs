﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class JobFoilagePlanting : OneTimeJob
    {
        internal PlantProduceBuilding Building { get; private set; }
        internal Point FoilagePosition { get; private set; }
        internal FoilageType FoilageType { get; private set; }
        internal Movable Worker { get; private set; }

        internal JobFoilagePlanting(Movable inWorker, Point inFoilagePosition, PlantProduceBuilding inBuilding, FoilageType inFoilageType)
            : base(inWorker)
        {
            if ((inWorker == null) || (inBuilding == null))
                throw new ArgumentNullException();

            Building = inBuilding;
            FoilagePosition = inFoilagePosition;
            Worker = inWorker;
            FoilageType = inFoilageType;
            AnimationClass = Building.Config.AnimationClass + "PlantWalking";
        }

        internal override bool Prepare()
        {
            if (!base.Prepare())
                return false;

            Point plantTreePos = new Point(FoilagePosition.X + 1, FoilagePosition.Y - 1);
            String buildingPrefix = Building.Config.AnimationClass;

            AddAnimationStep(plantTreePos, () =>
            {
                // walk to planting position
                VisualUtilities.Animate(Worker, buildingPrefix + "PlantWalking", inRestart: false, inRepeat: true);

                return true;
            }, null);

            int timePlanting = VisualUtilities.GetDurationMillis(Worker, buildingPrefix + "PlantTask");

            AddAnimationStepWithPathFollow(
                timePlanting,
                () =>
                {
                    // run plating animation
                    VisualUtilities.Animate(Worker, buildingPrefix + "PlantTask", inRestart: true, inRepeat: false);

                    return true;
                }, 
                (success) =>
                {
                    Building.Parent.ResMgr.AddFoilage(FoilagePosition, FoilageType, FoilageState.Growing);

                    return true;
                });

            // walk back to building
            AddAnimationStep(Building.SpawnPoint.Value.ToPoint(),
                () =>
                {
                    // switch to carrying animation
                    VisualUtilities.Animate(Worker, buildingPrefix + "PlantWalking", inRestart: true, inRepeat: true);

                    return true;
                },
                (succeeded) =>
                {
                    RaiseCompletion(succeeded);

                    return true;
                });

            return true;
        }
    }
}
