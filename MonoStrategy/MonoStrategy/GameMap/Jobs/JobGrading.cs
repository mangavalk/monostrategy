﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class JobGrading : GenericJob
    {
        internal BuildTask Task { get; private set; }

        internal JobGrading(Movable inMovable, BuildTask inTask)
            : base(inMovable)
        {
            if (inMovable.MovableType != MovableType.Grader)
                throw new ArgumentOutOfRangeException();

            if (inTask == null)
                throw new ArgumentNullException();

            Task = inTask;
        }

        internal override bool Prepare()
        {
            // job will repeat until building is graded
            if (Task.IsGraded)
            {
                // free grader from his occupation
                Dispose();

                return false;
            }

            Point target = new Point(
                Task.Building.Position.XGrid + m_Random.Next(0, Task.Config.GridWidth),
                Task.Building.Position.YGrid + m_Random.Next(0, Task.Config.GridHeight));

            // walk to next, randomly choosen grading spot
            AnimationClass = "GraderWalking";

            AddAnimationStep(target, null, null);

            // run grading animation
            AddAnimationStepWithPathFollow(1000,
                () => // onStarted
                {
                    AnimationClass = "GraderWorking";

                    return true;
                },
                (succeeded) => // onCompleted
                {
                    AnimationClass = "GraderWalking";
                    Task.DoGrade(target);

                    return true;
                });

            return true;
        }
    }
}
