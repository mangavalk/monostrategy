﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class JobCarrying : OneTimeJob
    {
        internal GenericResourceStack Provider { get; private set; }
        internal GenericResourceStack Query { get; private set; }
        internal Point CarryTo { get; private set; }
        internal event Procedure<JobCarrying> OnPickedUp;

        internal JobCarrying(Movable inMovable, GenericResourceStack inProvider, GenericResourceStack inQuery)
            : base(inMovable)
        {
            if ((inProvider == null) || (inQuery == null))
                throw new ArgumentNullException();

            Provider = inProvider;
            Query = inQuery;
            CarryTo = Query.Position.ToPoint();

            Provider.AddJob(Movable);
            Query.AddJob(Movable);
        }

        internal JobCarrying(Movable inMovable, GenericResourceStack inProvider, Point inCarryTo) 
            : base(inMovable)
        {
            if (inProvider == null)
                throw new ArgumentNullException();

            Provider = inProvider;
            CarryTo = inCarryTo;

            Provider.AddJob(Movable);
        }

        internal override bool Prepare()
        {
            if (!base.Prepare())
                return false;

            AddAnimationStep(Provider.Position.ToPoint(), null, (successProv) =>
            {
                Provider.RemoveJob(Movable);

                if(Query != null)
                    Query.RemoveJob(Movable);

                if (!successProv)
                {
                    Movable.Stop();

                    RaiseCompletion(false);

                    return false;
                }

                // pick up resource
                Provider.RemoveResource();
                Movable.Carrying = Provider.Resource;

                // walk to target
                if(Query != null)
                    Query.AddJob(Movable);

                if (OnPickedUp != null)
                    OnPickedUp(this);

                return true;
            });

            AddAnimationStep(CarryTo, null, (successQuery) =>
            {
                if (Query != null)
                    Query.RemoveJob(Movable);

                if (!successQuery || ((Query != null) && Query.IsRemoved))
                {
                    Movable.Stop();

                    RaiseCompletion(false);

                    return false;
                }

                // add resource to query
                if (Query != null)
                    Query.AddResource();

                Movable.Carrying = null;

                // finish job
                Movable.Stop();

                RaiseCompletion(true);

                return true;
            });

            return true;
        }
    }
}
