﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class JobConstructing : DirectionalJob
    {
        private Direction m_WorkingDirection;

        internal BuildTask Task { get; private set; }
        internal Point Position { get; private set; }

        internal JobConstructing(Movable inMovable, BuildTask inTask, Point inPosition, Direction inDirection)
            : base(inMovable)
        {
            if (inMovable.MovableType != MovableType.Constructor)
                throw new ArgumentOutOfRangeException();

            if (inTask == null)
                throw new ArgumentNullException();

            Task = inTask;
            m_WorkingDirection = inDirection;
            Position = inPosition;
        }

        internal override bool Prepare()
        {

            // job will repeat until building is built
            if (Task.IsBuilt)
            {
                // free constructor from his occupation
                Dispose();

                return false;
            }

            // walk to next, randomly choosen grading spot
            Direction = null;

            if(Movable.Position.ToPoint() != Position)
                AddAnimationStep(Position, null, null);

            // run grading animation
            AddAnimationStepWithPathFollow(1000,
                () => // onStarted
                {
                    Direction = m_WorkingDirection;
                    AnimationClass = "ConstructorWorking";

                    return true;
                },
                (succeeded) => // onCompleted
                {
                    Direction = null;
                    if(!Task.DoConstruct())
                        AnimationClass = "ConstructorWalking";

                    return true;
                });

            return true;
        }
    }
}
