﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoStrategy
{
    internal class ProfessionRecruting : OneTimeJob
    {
        internal SettlerProfessions Profession { get; private set; }
        internal GenericResourceStack Tool { get; private set; }

        internal ProfessionRecruting(Movable inSettler, GenericResourceStack inToolProvider, SettlerProfessions inProfession)
            : base(inSettler)
        {
            if (inToolProvider == null)
                throw new ArgumentNullException();

            Profession = inProfession;
            Tool = inToolProvider;
            AnimationClass = "SettlerWalking";

            Tool.AddJob(Movable);
        }

        internal override bool Prepare()
        {
            if (!base.Prepare())
                return false;

            AddAnimationStep(Tool.Position.ToPoint(), null, (successProv) =>
            {
                Tool.RemoveJob(Movable);

                if (!successProv)
                {
                    Movable.Stop();

                    RaiseCompletion(false);

                    return false;
                }

                // pick up resource
                Tool.RemoveResource();

                // transform movable to profession
                TransformMovable(Movable, Profession);

                Movable.Job = null;
                RaiseCompletion(true);

                return true;
            });

            return true;
        }

        internal static void TransformMovable(Movable inMovable, SettlerProfessions inTargetProfession)
        {
            // transform movable to profession
            switch (inTargetProfession)
            {
                case SettlerProfessions.Constructor: inMovable.MovableType = MovableType.Constructor; break;
                case SettlerProfessions.Grader: inMovable.MovableType = MovableType.Grader; break;
                default: throw new ApplicationException();
            }

            VisualUtilities.Animate(inMovable, inTargetProfession + "Walking", inRestart: false, inRepeat: true);
        }
    }
}
