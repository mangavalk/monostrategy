﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */

using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using MonoStrategy.RenderSystem;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    public enum ImageAlignment
    {
        Left,
        Center,
        Right,
    }

    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    public class Image : Control
    {
        private XMLGUIConfig.ImageEntry m_Config;
        private String m_SourceString;
        private int m_WidthOverride;
        private int m_HeightOverride;

        [XmlAttribute]
        public ImageAlignment HorizontalAlignment { get; set; }
        [XmlAttribute]
        public ImageAlignment VerticalAlignment { get; set; }
        [XmlAttribute]
        public Boolean KeepAspectRatio { get; set; }
        [XmlAttribute("Source")]
        public String SourceString
        {
            get
            {
                return m_SourceString;
            }
            set
            {
                m_SourceString = value;

                if (value.Contains("{") && value.Contains("}"))
                    return;

                m_Config = Program.GUIConfig.GetImage(value);

                ProcessScaling();
            }
        }

        public override void XMLPostProcess(XMLGUILayout inLayout)
        {
            base.XMLPostProcess(inLayout);

            ProcessScaling();
        }

        public Image() : base()
        {
        }

        public void ProcessScaling()
        {
            if (m_Config == null)
                return;

            Double aspect = m_Config.Image.Width / (double)m_Config.Image.Height;
            bool hasWidth = Width > 0;
            bool hasHeight = Height > 0;

            if (!hasWidth)
                m_WidthOverride = m_Config.Image.Width;
            else
                m_WidthOverride = Width;

            if (!hasHeight)
                m_HeightOverride = m_Config.Image.Height;
            else
                m_HeightOverride = Height;

            if (KeepAspectRatio && (hasWidth || hasHeight))
            {
                if (hasHeight ^ hasWidth)
                {
                    // exactly one dimension is not specified
                    if (hasHeight)
                        m_WidthOverride = (int)(Height * aspect);
                    else // hasWidth
                        m_HeightOverride = (int)(Width / aspect);
                }
                else
                {
                    // two dimensions are specified
                    m_WidthOverride = (int)(Height * aspect);
                    m_HeightOverride = (int)(Width / aspect);

                    if (m_WidthOverride > Width)
                    {
                        m_WidthOverride = Width;
                        m_HeightOverride = (int)(Width / aspect);
                    }

                    if (m_HeightOverride > Height)
                    {
                        m_HeightOverride = Height;
                        m_WidthOverride = (int)(Height * aspect);
                    }
                }
            }
        }

        protected override void Render(int inChainLeft, int inChainTop)
        {
            int left = Left, top = Top;

            switch (HorizontalAlignment)
            {
                case ImageAlignment.Left:  break;
                case ImageAlignment.Center: left = (Parent.Width - m_WidthOverride) / 2; break;
                case ImageAlignment.Right: left = Parent.Width - m_WidthOverride; break;
            }

            switch (VerticalAlignment)
            {
                case ImageAlignment.Left: break;
                case ImageAlignment.Center: top = (Parent.Height - m_HeightOverride) / 2; break;
                case ImageAlignment.Right: top = Parent.Height - m_HeightOverride; break;
            }

            Renderer.DrawSprite(
                     (inChainLeft + left) * WidthScale,
                     (inChainTop + top) * HeightScale,
                     m_WidthOverride * WidthScale,
                     m_HeightOverride * HeightScale,
                     m_Config.ImageID,
                     Opacity);

            base.Render(inChainLeft, inChainTop);
        }
    }
}
