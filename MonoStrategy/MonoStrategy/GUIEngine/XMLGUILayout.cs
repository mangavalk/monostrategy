﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [XmlRoot("GUILayout")]
    public class XMLGUILayout
    {
        [XmlAttribute]
        public String TemplatesPath { get; set; }
        public RootControl RootElement { get; set; }
        [XmlIgnore]
        public DateTime LastWriteTime { get; private set; }
        [XmlIgnore]
        public String ConfigDirectory { get; private set; }
        [XmlIgnore]
        private SortedDictionary<String, Template> m_Templates;

        public XMLGUILayout()
        {
            m_Templates = new SortedDictionary<string, Template>();
        }

        public static XMLGUILayout Load(String inXMLSource, FileInfo inFileInfo)
        {
            // load config XML
            XmlSerializer format = new XmlSerializer(typeof(XMLGUILayout));
            XMLGUILayout layoutXML = (XMLGUILayout)format.Deserialize(new StringReader(inXMLSource));

            layoutXML.ConfigDirectory = Path.GetDirectoryName(inFileInfo.FullName) + "/";
            layoutXML.LastWriteTime = inFileInfo.LastWriteTime;

            if (!String.IsNullOrEmpty(layoutXML.TemplatesPath))
            {
                // load templates
                String fullPath = Program.GetResourcePath("GUI/Default/" + layoutXML.TemplatesPath);
                String xmlContent = File.ReadAllText(fullPath);

                xmlContent = xmlContent.Replace("{Race}", Program.Map.Race.Name);

                var templateXML = XMLGUITemplates.Load(xmlContent, new FileInfo(fullPath));

                foreach (var template in templateXML.Entries)
                {
                    if (string.IsNullOrEmpty(template.Name))
                        throw new ArgumentException("A template name can not be null or empty.");

                    template.XMLPostProcess(layoutXML);

                    layoutXML.m_Templates.Add(template.Name, template);
                }
            }

            layoutXML.RootElement.XMLPostProcess(layoutXML);

            return layoutXML;
        }

        public Template GetTemplate(String inName)
        {
            if (!m_Templates.ContainsKey(inName))
                throw new KeyNotFoundException("A template named \"" + inName + "\" was not found.");

            return m_Templates[inName];
        }
    }
}
