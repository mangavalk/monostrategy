﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */

using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using MonoStrategy.RenderSystem;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    public class RootControl : Control
    {
        public RootControl() : base()
        {
        }

        public RootControl(params Control[] inControls)
            : base(inControls)
        {
        }

        public bool ProcessMouseUpEvent(int inMouseX, int inMouseY, int inMouseButton)
        {
            return ProcessMouseEvent(inMouseX, inMouseY, InvalidMouseButton, inMouseButton);
        }

        public bool ProcessMouseDownEvent(int inMouseX, int inMouseY, int inMouseButton)
        {
            return ProcessMouseEvent(inMouseX, inMouseY, inMouseButton, InvalidMouseButton);
        }

        public bool ProcessMouseMoveEvent(int inMouseX, int inMouseY)
        {
            return ProcessMouseEvent(inMouseX, inMouseY, InvalidMouseButton, InvalidMouseButton);
        }

        private bool ProcessMouseEvent(int inMouseX, int inMouseY, int inButtonDown, int inButtonUp)
        {
            if (Parent != null)
                throw new InvalidOperationException("For technical reasons, this call is allowed on root controls only.");

            if ((Renderer.ViewportWidth == 0) || (Renderer.ViewportHeight == 0))
                return false;

            SetupControlScales();

            // translate mouse coordinates back to internal representation
            int mouseX = (int)((inMouseX / (double)Renderer.ViewportWidth) / WidthScale);
            int mouseY = (int)((inMouseY / (double)Renderer.ViewportHeight) / HeightScale);

            return ProcessMouseEventInternal(mouseX, mouseY, inButtonDown, inButtonUp);
        }

        private void SetupControlScales()
        {
            Double heightRatio = Renderer.ViewportHeight / (double)Height;
            Control.HeightScale = 1.0 / (double)Height;
            Control.WidthScale = 1.0 / (double)Renderer.ViewportWidth * heightRatio;
        }

        public void Render()
        {
            if (Parent != null)
                throw new InvalidOperationException("For technical reasons, this call is allowed on root controls only.");

            SetupControlScales();

            Render(0, 0);
        }
    }
}
