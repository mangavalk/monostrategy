﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [XmlRoot("GUITemplates")]
    public class XMLGUITemplates
    {
        [XmlElement("Template")]
        public List<Template> Entries { get; set; }
        [XmlIgnore]
        public DateTime LastWriteTime { get; private set; }
        [XmlIgnore]
        public String ConfigDirectory { get; private set; }

        public static XMLGUITemplates Load(String inXMLContent, FileInfo inFileInfo)
        {
            // load config XML
            XmlSerializer format = new XmlSerializer(typeof(XMLGUITemplates));
            XMLGUITemplates templateXML = (XMLGUITemplates)format.Deserialize(new StringReader(inXMLContent));

            templateXML.ConfigDirectory = Path.GetDirectoryName(inFileInfo.FullName) + "/";
            templateXML.LastWriteTime = inFileInfo.LastWriteTime;

            return templateXML;
        }
    }
}
