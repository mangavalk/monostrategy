﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */

using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using MonoStrategy.RenderSystem;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    public class RadioButton : Button
    {
        protected virtual void CheckButton()
        {
            // reset all other TabButtons within this control level
            foreach (var btn in Parent.Children)
            {
                RadioButton radio = btn as RadioButton;

                if ((radio == null) || (radio == this))
                    continue;

                if (radio.IsDown)
                    radio.DoClick(true);
            }
        }

        protected virtual void UncheckButton()
        {
        }

        protected override void DoClick(Boolean inIsSynthetic)
        {
            if (!inIsSynthetic)
            {
                if (!IsDown)
                {
                    // Button can only be actively pressed
                    base.DoClick(inIsSynthetic);

                    CheckButton();
                }
                // else: do nothing!
            }
            else
            {
                if (!IsDown)
                {
                    CheckButton();
                }
                else
                {
                    IsDown = false;

                    UncheckButton();
                }
            }
        }
    }
}
