﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using MonoStrategy.RenderSystem;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    public class TabButton : RadioButton
    {
        [XmlAttribute("Tab")]
        public String TabString { get; set; }
        [XmlIgnore]
        public Control Tab { get; private set; }

        public TabButton()
            : base()
        {

        }

        protected override void CheckButton()
        {
            base.CheckButton();

            // make tab visible
            if (Tab != null)
            {
                Tab.IsVisible = true;
            }
        }

        protected override void UncheckButton()
        {
            base.UncheckButton();

            // make tab invisible
            if (Tab != null)
            {
                Tab.IsVisible = false;
            }
        }

        public override void XMLPostProcess(XMLGUILayout inLayout)
        {
            base.XMLPostProcess(inLayout);

            if (!String.IsNullOrEmpty(TabString))
            {
                Tab = RootElement.FindControl(TabString);

                if (Tab == null)
                    throw new ArgumentException("A control with name \"" + TabString + "\" does not exist.");
            }
        }

        
    }
}
