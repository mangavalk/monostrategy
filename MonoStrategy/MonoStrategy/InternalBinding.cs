﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace MonoStrategy
{
    internal class InternalBinding<T> : BindingList<T>
    {
        private Boolean m_AllowChange = true;

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            m_AllowChange = true;
        }

        internal void Protect()
        {
            m_AllowChange = false;
        }

        internal void AddInternal(T inItem)
        {
            try
            {
                m_AllowChange = true;

                Add(inItem);
            }
            finally
            {
                m_AllowChange = false;
            }
        }

        internal void InsertInternal(Int32 inIndex, T inItem)
        {
            try
            {
                m_AllowChange = true;

                Insert(inIndex, inItem);
            }
            finally
            {
                m_AllowChange = false;
            }
        }

        internal void ClearInternal()
        {
            try
            {
                m_AllowChange = true;

                Clear();
            }
            finally
            {
                m_AllowChange = false;
            }
        }

        internal Boolean RemoveInternal(T inItem)
        {
            try
            {
                m_AllowChange = true;

                return Remove(inItem);
            }
            finally
            {
                m_AllowChange = false;
            }
        }

        internal void RemoveAtInternal(Int32 inIndex)
        {
            try
            {
                m_AllowChange = true;

                RemoveAt(inIndex);
            }
            finally
            {
                m_AllowChange = false;
            }
        }

        protected override object AddNewCore()
        {
            if (!m_AllowChange)
                throw new InvalidOperationException("List is in read-only state and can not be modified!");

            return base.AddNewCore();
        }

        protected override void ClearItems()
        {
            if (!m_AllowChange)
                throw new InvalidOperationException("List is in read-only state and can not be modified!");

            base.ClearItems();
        }

        protected override void InsertItem(int index, T item)
        {
            if (!m_AllowChange)
                throw new InvalidOperationException("List is in read-only state and can not be modified!");

            base.InsertItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            if (!m_AllowChange)
                throw new InvalidOperationException("List is in read-only state and can not be modified!");

            base.RemoveItem(index);
        }
    }
}
