﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using OpenTK;
using OpenTK.Input;

using System.Runtime.InteropServices;
using System.IO;

#if EMBEDDED
    using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.OpenGL;
#endif

namespace MonoStrategy.RenderSystem
{
    public partial class TerrainRenderer
    {

        /// <summary>
        /// "Internal" method used by AnimationEditor.
        /// </summary>
        public MovableOrientedAnimation CreateMovableOrientedAnimation(AnimationClass inAnimClass, Movable inMovable, Double inWidth)
        {
            var visual = new MovableOrientedAnimation(this, inAnimClass, inMovable);

            lock (m_SceneObjects)
            {
                m_SceneObjects.Add(visual);
            }

            return visual;
        }


        public MovableOrientedAnimation CreateMovableOrientedAnimation(String inAnimClass, Movable inMovable, Double inWidth)
        {
            return CreateMovableOrientedAnimation(AnimationLibrary.Instance.FindClass(inAnimClass), inMovable, inWidth);
        }

        /// <summary>
        /// "Internal" method used by AnimationEditor.
        /// </summary>
        public RenderableAnimationClass CreateAnimation(
            AnimationClass inAnimClass, 
            IPositionTracker inPositionTracker, 
            Double inOpacity)
        {
            if (inPositionTracker == null)
                throw new ArgumentNullException();

            var visual = new RenderableAnimationClass(this, inAnimClass, inPositionTracker);

            visual.Opacity = inOpacity;

            lock (m_SceneObjects)
            {
                m_SceneObjects.Add(visual);
            }

            return visual;
        }

        public RenderableAnimationClass CreateAnimation(String inAnimClass, IPositionTracker inPositionTracker, Double inOpacity)
        {
            return CreateAnimation(AnimationLibrary.Instance.FindClass(inAnimClass), inPositionTracker, inOpacity);
        }

        public RenderableAnimationClass CreateAnimation(String inAnimClass, IPositionTracker inPositionTracker)
        {
            return CreateAnimation(AnimationLibrary.Instance.FindClass(inAnimClass), inPositionTracker, 1.0);
        }

        public void RemoveVisual(RenderableVisual inVisual)
        {
            if (inVisual == null)
                throw new ArgumentNullException();

            lock (m_SceneObjects)
            {
                m_SceneObjects.Remove(inVisual);
            }
        }
    }
}
