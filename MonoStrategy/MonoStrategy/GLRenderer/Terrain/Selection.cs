﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;

#if EMBEDDED
    using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.OpenGL;
#endif

namespace MonoStrategy.RenderSystem
{
    public partial class TerrainRenderer
    {
        private void UpdateObjectSelection(IEnumerable<RenderableVisual> visibleObjects)
        {
            /*######################################################
             * Update selection for objects
             *#####################################################*/
            m_SelectionPassResults.Clear();
            {
                GL.ClearColor(Color.Black);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                // setup GLSL program
                m_2DSceneSelectProgram.Bind(m_ProjMatrix, m_ViewMatrix, m_ModelMatrix);

                // selection pass (each one is rendered with an unique color)
                m_IsSelectionPass = true;
                {
                    RadixRenderSprites(visibleObjects);
                }
                m_IsSelectionPass = false;

                // process selection information
                int[] selectedPixels = new int[1];
                int selValue;
                var prevMouseOver = MouseOverVisual;

                GL.ReadPixels(
                    Renderer.MouseXY.X,
                    Renderer.ViewportHeight - Renderer.MouseXY.Y,
                    1, 1, PixelFormat.Rgb, PixelType.UnsignedByte,
                    selectedPixels);

                if (selectedPixels[0] > 0)
                {
                    selValue = (selectedPixels[0] / SelectionGranularity) - 1;

                    if ((selValue >= 0) && (selValue < m_SelectionPassResults.Count))
                        MouseOverVisual = m_SelectionPassResults[selValue];
                    else
                        MouseOverVisual = null;
                }
                else
                    MouseOverVisual = null;

                if (prevMouseOver != MouseOverVisual)
                {
                    // raise selection events
                    var newOver = MouseOverVisual;

                    ThreadPool.QueueUserWorkItem((unused) =>
                    {
                        var mouseLeave = OnMouseLeave;
                        var mouseEnter = OnMouseEnter;

                        if ((mouseLeave != null) && (prevMouseOver != null))
                            mouseLeave(this, prevMouseOver);

                        if ((mouseEnter != null) && (newOver != null))
                            mouseEnter(this, newOver);
                    });
                }
            }

            /*######################################################
             * Update selection for terrain
             *#####################################################*/
            Point gridPos;

            MouseToGridPos(Renderer.MouseXY, m_ProjMatrix, m_ViewMatrix, m_ModelMatrix, out gridPos);

            if (GridXY != gridPos)
            {
                GridXY = gridPos;

                if (OnMouseGridMove != null)
                    OnMouseGridMove(this, GridXY);
            }
        }
    }
}
