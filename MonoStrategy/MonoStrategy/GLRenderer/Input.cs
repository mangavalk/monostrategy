﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using OpenTK;
using OpenTK.Input;

using System.Runtime.InteropServices;

#if EMBEDDED
    using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.OpenGL;
#endif

namespace MonoStrategy.RenderSystem
{
    public partial class GLRenderer
    {
        private void RaiseMouseMove(object sender, MouseMoveEventArgs args)
        {
            int inNewMouseX = args.X;
            int inNewMouseY = args.Y;

            inNewMouseX = Math.Max(0, Math.Min(inNewMouseX, ViewportWidth - 1));
            inNewMouseY = Math.Max(0, Math.Min(inNewMouseY, ViewportWidth - 1));

            MouseXY = new Point(inNewMouseX, inNewMouseY);

            if (OnMouseMove != null)
                OnMouseMove(this, new Point(inNewMouseX, inNewMouseY));
        }

        private void RaiseKeyboardKeyUp(object sender, KeyboardKeyEventArgs e)
        {
            lock (m_KeyboardState)
            {
                m_KeyboardState.Remove(e.Key);
            }

            if (OnKeyUp != null)
                OnKeyUp(this, e.Key);
        }

        private void RaiseKeyboardKeyDown(object sender, KeyboardKeyEventArgs e)
        {
            bool hasChanged = false;

            lock (m_KeyboardState)
            {
                hasChanged = !m_KeyboardState.Contains(e.Key);

                if(hasChanged)
                    m_KeyboardState.Add(e.Key);
            }

            if (hasChanged && (OnKeyDown != null))
                OnKeyDown(this, e.Key);
        }

        private void RaiseKeyboardKeyRepeat()
        {
            if (OnKeyRepeat == null)
                return;

            foreach (var key in m_KeyboardState)
            {
                OnKeyRepeat(this, key);
            }
        }

        private void RaiseMouseButtonUp(object sender, MouseButtonEventArgs e)
        {
            int btn = (int)e.Button;

            lock (m_MouseState)
            {
                m_MouseState.Remove(btn);
            }

            if (OnMouseUp != null)
                OnMouseUp(this, btn);
        }

        private void RaiseMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            int btn = (int)e.Button;

            lock (m_MouseState)
            {
                m_MouseState.Add(btn);
            }

            if (OnMouseDown != null)
                OnMouseDown(this, btn);
        }
    }
}
