﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OpenTK;
using OpenTK.Input;


#if EMBEDDED
    using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.OpenGL;
#endif

namespace MonoStrategy.RenderSystem
{
    /// <summary>
    /// Provides convenient access to GLSL shaders. As with other OpenGL related 
    /// unmanaged resources, you MUST dispose any allocated instance explicitly,
    /// otherwise you will get an exception by the time such an leaking object is GCed.
    /// Also see <see cref="GLProgram"/>.
    /// </summary>
    internal class GLShader : IDisposable
    {
        private int? m_ShaderID;

        /// <summary>
        /// Vertex- or pixelshader?
        /// </summary>
        public ShaderType Type { get; private set; }
        /// <summary>
        /// Source code for the shader.
        /// </summary>
        public String Source { get; private set; }
        /// <summary>
        /// OpenGL specific shader ID.
        /// </summary>
        public int ShaderID { get { return m_ShaderID.Value; } }

        /// <summary>
        /// A resource leak check. Due to wrong thread context, we usually can't release OpenGL resources
        /// in class destructors!
        /// </summary>
        ~GLShader()
        {
            //if (m_ShaderID.HasValue)
            //    Log.LogExceptionCritical(new ArgumentException("Shader has not been released before GC."));
        }

        /// <summary>
        /// Releases all unmanaged resources associated with this shader.
        /// </summary>
        public void Dispose()
        {
            if (!m_ShaderID.HasValue)
                return;

            GL.DeleteShader(ShaderID); GLRenderer.CheckError();

            m_ShaderID = null;
        }

        /// <summary>
        /// Loads a new shader from file. For performance reasons, shaders might be
        /// cached later on, but this won't have an impact on this API.
        /// </summary>
        /// <param name="inFileName">A file containing the shader source code.</param>
        public GLShader(String inFileName) : this(inFileName, File.ReadAllText(inFileName))
        {

        }

        public GLShader(String inFileName, String inSourceCode)
        {
            try
            {
                if (inFileName.EndsWith(".vert"))
                    Type = ShaderType.VertexShader;
                else if (inFileName.EndsWith(".frag"))
                    Type = ShaderType.FragmentShader;
                else
                    throw new ArgumentException();

                Source = inSourceCode;

                String log;

                try
                {
                    m_ShaderID = GL.CreateShader(Type); GLRenderer.CheckError();
                    GL.ShaderSource(ShaderID, Source); GLRenderer.CheckError();
                    GL.CompileShader(ShaderID); GLRenderer.CheckError();

                    log = GL.GetShaderInfoLog(ShaderID);
                }
                catch (Exception e)
                {
                    throw new ArgumentException("Shader \"" + inFileName + "\" failed to load.", e);
                }

                int success;

                GL.GetShader(ShaderID, ShaderParameter.CompileStatus, out success);
                    
                if(success != 1)
                    throw new ArgumentException("Shader \"" + inFileName + "\" failed to compile: \"" + log + "\".");
                    
            }
            catch (Exception e)
            {
                Dispose();

                throw e;
            }
        }
    }
}
