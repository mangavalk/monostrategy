﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */

using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    [ObfuscationAttribute(Feature = "renaming", ApplyToMembers = true)]
    [XmlRoot("MonoStrategy")]
    public class XMLConfig 
    {
        [XmlIgnore]
        public String ConfigPath { get; private set; }
        [XmlElement("ResourcePath")]
        public String OriginalResourcePath { get; set; }
        [XmlIgnore]
        public String ResourcePath { get; set; }
        public String S3CheckOverride { get; set; }
        public String S3InstallPath { get; set; }
        public GLRenderConfig GLRenderer { get; set; }
        public Boolean UseMinimalBounds { get; set; }

        public static XMLConfig Load(String inFileName)
        {
            // load config XML
            XmlSerializer format = new XmlSerializer(typeof(XMLConfig));
            Stream stream = File.OpenRead(inFileName);
            XMLConfig configXML;

            using (stream)
            {
                configXML = (XMLConfig)format.Deserialize(stream);

                configXML.ConfigPath = new FileInfo(inFileName).FullName;
                configXML.ResourcePath = configXML.OriginalResourcePath;
            }

            // validate config
            configXML.ProcessResourcePath();
            configXML.ProcessGLRenderer();

            return configXML;
        }

        public static void Save(XMLConfig inConfig, String inFileName)
        {
            // load config XML
            XmlSerializer format = new XmlSerializer(typeof(XMLConfig));
            Stream stream = File.OpenWrite(inFileName);

            using (stream)
            {
                stream.SetLength(0);
                format.Serialize(stream, inConfig);
            }
        }

        private void ProcessResourcePath()
        {
            String backup = ResourcePath;

            if (String.IsNullOrEmpty(ResourcePath))
                ResourcePath = "./Resources/";

            if (!Path.IsPathRooted(ResourcePath))
            {
                // paths are threaded relative to config file directory...
                ResourcePath = Path.GetFullPath(Path.GetDirectoryName(ConfigPath) + "/" + ResourcePath);
            }

            if (!Directory.Exists(ResourcePath))
                throw new DirectoryNotFoundException("Resource directory \"" + ResourcePath + "\" could not be found. (Original string was \"" + backup + "\" relative to its config file \"" + ConfigPath + "\").");
        }

        private void ProcessGLRenderer()
        {
            if (GLRenderer == null)
                GLRenderer = new GLRenderConfig();
        }
    }
}
