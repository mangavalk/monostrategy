﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MonoStrategy
{
    public static partial class Program
    {
        private static void CheckSettlersInstallation()
        {
            if (Config.S3CheckOverride == "FE67AC3DB1D928EF9C3")
                return;

            
            // check for S3 Installation
            String errStr = "";

            if (!String.IsNullOrEmpty(Config.S3InstallPath))
                errStr = CheckS3InstallPath();

            if (errStr != null)
            {
                MessageBox.Show(
                    "This is just a very first demo! Due to license restrictions it is required to have a valid installation " +
                    "of \"The Settlers 3\" (Ubisoft/Bluebyte). We'll get rid of this requirement when replacing all the media " +
                    "with our own. If you are a 3D artist and want to help, please contact us at \"monostrategy.codeplex.com\". " +
                    "After clicking \"OK\" you need to provide the path to your Settlers 3 executable, usually located at " +
                    "somehting like \"C:\\BlueByte\\Settlers 3\\s3.exe\". We apologize for this inconvenience! If you don't have " +
                    "a valid S3 installation, you could check out our YouTube-Video demonstrating the current gameplay.",
                    "MonoStrategy MILESTONE 0 - Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                OpenFileDialog dlg = new OpenFileDialog();

                dlg.CheckFileExists = true;
                dlg.Filter = "Settlers 3 EXE|*.exe";
                dlg.Multiselect = false;
                dlg.ReadOnlyChecked = true;
                dlg.Title = "Enter path to S3.exe!";

                var res = dlg.ShowDialog();

                if ((res != DialogResult.OK) || !File.Exists(dlg.FileName) || !dlg.FileName.EndsWith("s3.exe"))
                {
                    MessageBox.Show("No valid path to S3-EXE was given! Due to license restriction, the application will now abort.", "Missing license...",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    Log.Exit();
                }

                Config.S3InstallPath = Path.GetDirectoryName(Path.GetFullPath(dlg.FileName));
            }

            if ((errStr = CheckS3InstallPath()) != null)
            {
                MessageBox.Show(errStr, "Missing license...",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                Log.Exit();
            }
        }

        private static String CheckS3InstallPath()
        {
            String s3Dir = Config.S3InstallPath;

            // scan for GFX files
            String[] gfxFiles = new string[] { 
                    "Siedler3_01.f8007e01f",
                    "Siedler3_02.f8007e01f",
                    "Siedler3_03.f8007e01f",
                    "Siedler3_04.f8007e01f",
                    "Siedler3_06.f8007e01f",
                    "Siedler3_10.f8007e01f",
                    "Siedler3_11.f8007e01f",
                    "Siedler3_12.f8007e01f",
                    "Siedler3_13.f8007e01f",
                    "Siedler3_14.f8007e01f",
                };

            foreach (var file in gfxFiles)
            {
                String filename = s3Dir + "/GFX/" + file + ".dat";

                if (!File.Exists(filename))
                {
                    return "Missing GFX file \"" + filename + "\".";
                }
            }

            return null;
        }
    }
}
