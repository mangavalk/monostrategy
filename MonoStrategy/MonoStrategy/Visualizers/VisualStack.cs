﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    public class VisualStack
    {
        public TerrainRenderer Renderer { get; private set; }
        public GenericResourceStack Stack { get; private set; }
        public RenderableAnimationClass Visual { get; private set; }

        private VisualStack(TerrainRenderer inRenderer, GenericResourceStack inStack)
        {
            if ((inRenderer == null) || (inStack == null))
                throw new ArgumentNullException();

            Stack = inStack;
            Renderer = inRenderer;
            Stack.UserContext = this;

            Visual = inRenderer.CreateAnimation("ResourceStacks", inStack, 2.0);
            Visual.UserContext = this;
            inStack.OnCountChanged += Stack_OnCountChanged;

            Visual.Play(inStack.Resource.ToString());

            Stack_OnCountChanged(inStack, 0, 0);
        }

        void Stack_OnCountChanged(GenericResourceStack inSender, int inOldValue, int inNewValue)
        {
            int avail = inSender.Available;

            if (avail > 0)
            {
                Visual.FrozenFrameIndex = avail - 1;

                Visual.IsVisible = true;
            }
            else
                Visual.IsVisible = false;
        }

        public static void Assign(TerrainRenderer inRenderer, GenericResourceStack inStack)
        {
            if (inStack.UserContext != null)
                throw new InvalidOperationException();

            new VisualStack(inRenderer, inStack);
        }

        public static void Detach(GenericResourceStack inStack)
        {
            if (inStack.UserContext == null)
                return;

            VisualStack visualizer = inStack.UserContext as VisualStack;

            if (visualizer.Visual != null)
                visualizer.Renderer.RemoveVisual(visualizer.Visual);

            inStack.UserContext = null;
        }
    }
}
