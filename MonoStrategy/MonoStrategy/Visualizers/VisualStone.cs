﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    public class VisualStone
    {
        public TerrainRenderer Renderer { get; private set; }
        public Stone Stone { get; private set; }
        public RenderableAnimationClass Visual { get; private set; }

        private VisualStone(TerrainRenderer inRenderer, Stone inStone)
        {
            if ((inRenderer == null) || (inStone == null))
                throw new ArgumentNullException();

            Stone = inStone;
            Renderer = inRenderer;
            Stone.UserContext = this;

            var anim = Renderer.CreateAnimation("Stone", Stone);

            Visual = anim;
            Visual.UserContext = this;
            Stone.OnStateChanged += Stone_OnStateChanged;

            Stone_OnStateChanged(Stone);
        }

        private void Stone_OnStateChanged(Stone inParam)
        {
            VisualUtilities.Animate(Stone);
        }

        public static void Assign(TerrainRenderer inRenderer, Stone inStone)
        {
            if (inStone.UserContext != null)
                throw new InvalidOperationException();

            new VisualStone(inRenderer, inStone);
        }

        public static void Detach(Stone inStone)
        {
            if (inStone.UserContext == null)
                return;

            VisualStone visualizer = inStone.UserContext as VisualStone;

            if (visualizer.Visual != null)
                visualizer.Renderer.RemoveVisual(visualizer.Visual);

            inStone.UserContext = null;
        }
    }
}
