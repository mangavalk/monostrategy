﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    public class VisualMovable
    {
        public TerrainRenderer Renderer { get; private set; }
        public Movable Movable { get; private set; }
        public MovableOrientedAnimation Visual { get; set; }

        private VisualMovable(TerrainRenderer inRenderer, Movable inMovable)
        {
            if ((inRenderer == null) || (inMovable == null))
                throw new ArgumentNullException();

            Movable = inMovable;
            Renderer = inRenderer;
            Movable.UserContext = this;

            Movable.OnJobChanged += Settler_OnJobChanged;

            Settler_OnJobChanged(Movable, null, Movable.Job);
        }

        public static void Assign(TerrainRenderer inRenderer, Movable inMovable)
        {
            if (inMovable.UserContext != null)
                throw new InvalidOperationException();

            new VisualMovable(inRenderer, inMovable);
        }

        public static void Detach(Movable inMovable)
        {
            if (inMovable.UserContext == null)
                return;

            VisualMovable visualizer = inMovable.UserContext as VisualMovable;

            if (visualizer.Visual != null)
                visualizer.Renderer.RemoveVisual(visualizer.Visual);

            visualizer.Visual = null;
            inMovable.UserContext = null;
        }

        private static String GetDefaultAnimClass(Movable inMovable)
        {
            switch (inMovable.MovableType)
            {
                case MovableType.Settler: return "SettlerWalking"; 
                case MovableType.Grader: return "GraderWalking"; 
                case MovableType.Constructor: return "ConstructorWalking"; 
                default: throw new ApplicationException();
            }
        }

        private static void Settler_OnJobChanged(Movable inMovable, GenericJob inOldJob, GenericJob inNewJob)
        {
            String defaultAnimClass = GetDefaultAnimClass(inMovable);

            if(inOldJob != null)
                inOldJob.OnAnimationClassChanged -= SettlerJob_OnAnimationClassChanged;

            if (inNewJob != null)
            {
                inNewJob.OnAnimationClassChanged += SettlerJob_OnAnimationClassChanged;

                if (inNewJob is DirectionalJob)
                {
                    var dirJob = inNewJob as DirectionalJob;

                    VisualUtilities.DirectedAnimation(inMovable, String.IsNullOrEmpty(inNewJob.AnimationClass) ? defaultAnimClass : inNewJob.AnimationClass, dirJob.Direction, inRestart: false, inRepeat: true);
                }
                else
                    VisualUtilities.Animate(inMovable, String.IsNullOrEmpty(inNewJob.AnimationClass) ? defaultAnimClass : inNewJob.AnimationClass, inRestart: false, inRepeat: true);
            }
            else
            {
                VisualUtilities.Animate(inMovable, defaultAnimClass, inRestart: false, inRepeat: true);
            }
        }

        private static void SettlerJob_OnAnimationClassChanged(GenericJob inParam)
        {
            String defaultAnimClass = GetDefaultAnimClass(inParam.Movable);

            if (inParam is DirectionalJob)
            {
                var dirJob = inParam as DirectionalJob;

                VisualUtilities.DirectedAnimation(inParam.Movable, String.IsNullOrEmpty(inParam.AnimationClass) ? defaultAnimClass : inParam.AnimationClass, dirJob.Direction, inRestart: false, inRepeat: true);
            }
            else
                VisualUtilities.Animate(inParam.Movable, String.IsNullOrEmpty(inParam.AnimationClass) ? defaultAnimClass : inParam.AnimationClass, inRestart: false, inRepeat: true);
        }
    }
}
