﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    public class VisualBuilding
    {
        public TerrainRenderer Renderer { get; private set; }
        public BaseBuilding Building { get; private set; }
        public RenderableAnimationClass Visual { get; private set; }

        private VisualBuilding(TerrainRenderer inRenderer, BaseBuilding inBuilding)
        {
            if ((inRenderer == null) || (inBuilding == null))
                throw new ArgumentNullException();

            Building = inBuilding;
            Renderer = inRenderer;

            Visual = inRenderer.CreateAnimation(
                inBuilding.Config.AnimationClass,
                inBuilding);
            Visual.UserContext = this;

            // all resource stacks should inherit the building's ZShift
            foreach (var prov in inBuilding.ProvidersReadonly)
            {
                if ((prov == null) || (prov.UserContext == null))
                    continue;

                ((VisualStack)prov.UserContext).Visual.ZShiftOverride = Visual.GetZShift();
            }

            foreach (var query in inBuilding.QueriesReadonly)
            {
                if ((query == null) || (query.UserContext == null))
                    continue;

                ((VisualStack)query.UserContext).Visual.ZShiftOverride = Visual.GetZShift();
            }

            inBuilding.OnStateChanged += (building) =>
            {
                if (building.IsReady)
                    Visual.Play("Flag");
            };

            if (inBuilding.IsReady)
                Visual.Play("Flag");
        }

        public static void Assign(TerrainRenderer inRenderer, BaseBuilding inBuilding)
        {
            if (inBuilding.UserContext != null)
                throw new InvalidOperationException();

            inBuilding.UserContext = new VisualBuilding(inRenderer, inBuilding);
        }

        public static void Detach(BaseBuilding inBuilding)
        {
            if (inBuilding.UserContext == null)
                return;

            VisualBuilding visualizer = inBuilding.UserContext as VisualBuilding;

            if (visualizer.Visual != null)
                visualizer.Renderer.RemoveVisual(visualizer.Visual);

            inBuilding.UserContext = null;
        }
    }
}
