﻿/*
 * This file is part of MonoStrategy.
 *
 * Copyright (C) 2010-2011 Christoph Husse
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: 
 *      # Christoph Husse
 * 
 * Also checkout our homepage: http://monostrategy.codeplex.com/
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonoStrategy.RenderSystem;

namespace MonoStrategy
{
    public class VisualFoilage
    {
        public TerrainRenderer Renderer { get; private set; }
        public Foilage Foilage { get; private set; }
        public RenderableAnimationClass Visual { get; private set; }

        private VisualFoilage(TerrainRenderer inRenderer, Foilage inFoilage)
        {
            if ((inRenderer == null) || (inFoilage == null))
                throw new ArgumentNullException();

            Foilage = inFoilage;
            Renderer = inRenderer;
            Foilage.UserContext = this;

            Visual = Renderer.CreateAnimation(inFoilage.Type.ToString(), Foilage);
            Visual.UserContext = this;
            Foilage.OnStateChanged += Foilage_OnStateChanged;

            Foilage_OnStateChanged(Foilage);
        }

        private void Foilage_OnStateChanged(Foilage inParam)
        {
            String animSet;
            bool repeat = true;

            switch (inParam.State)
            {
                case FoilageState.Growing: animSet = "Growing"; repeat = false; break;
                case FoilageState.Grown: animSet = "Ambient"; break;
                default: return;
            }

            VisualUtilities.Animate(inFoilage: Foilage, inState: animSet, inRestart: true, inRepeat: repeat);
        }

        public static void Assign(TerrainRenderer inRenderer, Foilage inFoilage)
        {
            if (inFoilage.UserContext != null)
                throw new InvalidOperationException();

            new VisualFoilage(inRenderer, inFoilage);
        }

        public static void Detach(Foilage inFoilage)
        {
            if (inFoilage.UserContext == null)
                return;

            VisualFoilage visualizer = inFoilage.UserContext as VisualFoilage;

            if (visualizer.Visual != null)
                visualizer.Renderer.RemoveVisual(visualizer.Visual);

            inFoilage.UserContext = null;
        }
    }
}
